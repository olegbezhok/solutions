﻿using NET01._1.Essences;
using System;

namespace NET01._1.Helpers
{
    public static class IdGenerator
    {
        public static void GenerateId(this Entity essence)
        {
            essence.Id = Guid.NewGuid();
        }
    }
}
