﻿using System;
using NET01._1.Enums;
using NET01._1.Essences;
using NET01._1.Essences.Lessons;
using NET01._1.Essences.Materials;
using NET01._1.Helpers;

namespace NET01._1
{
    class Program
    {
        static void Main(string[] args)
        {
            var refer = new ReferenceMaterial(ReferenceType.Html, "sadffaaf");
            Lesson lesson = new Lesson(new Material[]
            {
                refer, new TextMaterial("sdada", "sadsafas") 
            },
            new byte[]{
                1,2,3,4,5,6,7
            }, "asdafafsada");
            lesson.Version = new byte[]
            {
                1,2,3,4,5,6,7
            };
            lesson.Description = null;
            lesson.Description = "sadasfasfasdsa";
            Console.WriteLine(lesson.ToString());
            lesson.GenerateId();
            Console.WriteLine(lesson.Id);

            var newClone = (Lesson)lesson.Clone();

            Console.WriteLine(newClone.Equals(lesson));
            
            newClone.Description = "Oleg";
            refer.Description = "Oleg";
            Console.WriteLine(newClone.Description);
            Console.WriteLine(lesson.Description);


            Console.WriteLine(newClone.Equals(lesson));

            for (int i = 0; i < lesson.Version.Length; i++)
            {
                Console.Write(lesson.Version[i]);
            }

            Console.ReadKey();
        }
    }
}
