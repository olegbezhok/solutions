﻿namespace NET01._1.Interfaces
{
    interface IVersionable
    {
        byte[] Version { get; set; }
    }
}
