using System;
using System.Linq;
using NET01._1.Enums;
using NET01._1.Essences.Materials;
using NET01._1.Interfaces;

namespace NET01._1.Essences.Lessons
{
    public class Lesson : Entity, IVersionable, ICloneable
    {
        private const int _versionLength = 8;

        private Material[] _materials = new Material[0];
        private byte[] _version;

        public LessonType GetLessonType(Lesson lesson)
        {
            for (int i = 0; i < lesson._materials.Length; i++)
            {
                if (lesson._materials[i] is VideoMaterial)
                {
                    return LessonType.VideoLesson;
                }
            }

            return LessonType.TextLesson;
        }

        public byte[] Version
        {
            get { return _version; }
            set
            {
                if (value.Length > _versionLength)
                {
                    throw new ArgumentException($"version length can't be more than {_versionLength}");
                }
                else
                {
                    _version = value;
                }
            }
        }

        private Lesson()
        {
        }

        public Lesson(Material[] materials, byte[] version, string descrtiption) : base(descrtiption)
        {
            Version = version;
            _materials = materials;
        }

        public object Clone()
        {
            var lesson = new Lesson
            {
                _version = this._version,
                Description = this.Description,
                Id = this.Id,
                _materials = new Material[this._materials.Length]
            };

            for (int i = 0; i < this._materials.Length; i++)
            {
                lesson._materials[i] = (Material)this._materials[i].Clone();
            }

            return lesson;
        }
    }
}
