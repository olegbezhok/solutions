﻿using System;
using System.Collections.Generic;

namespace NET01._1.Essences
{
    public abstract class Entity
    {
        private const int DescriptionLength = 256;

        private string _description;

        public Guid Id { get; set; }

        public string Description
        {
            get { return _description; }
            set
            {
                if (value != null && value.Length > DescriptionLength)
                {
                    throw new ArgumentException($"Description length can't be more than{DescriptionLength}");
                }

                _description = value;
            }
        }

        public Entity(string description)
        {
            this._description = description;
        }

        public Entity()
        {

        }


        public override bool Equals(object value)
        {
            var essence = value as Entity;

            if (essence != null)
            {
                return Id == essence.Id;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override string ToString()
        {
            return _description;
        }
    }
}
