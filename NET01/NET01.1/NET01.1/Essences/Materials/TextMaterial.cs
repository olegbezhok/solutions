﻿using System;

namespace NET01._1.Essences.Materials
{
    public class TextMaterial : Material
    {
        private const int TextLength = 1000;

        private string _text;

        public string Text
        {
            get { return _text; }
            set
            {
                if (value.Length > TextLength || string.IsNullOrEmpty(value))
                {
                    throw new Exception($"Text length can't be more than {TextLength} or must have some text");
                }

                _text = value;
            }
        }

        public TextMaterial(string text, string description) : base(description)
        {
            Text = text;
        }

        private TextMaterial()
        {
        }

        public override object Clone()
        {
            return new TextMaterial
                {
                    Text = this.Text,
                    Description = this.Description,
                    Id = this.Id
                };
        }
    }
}
