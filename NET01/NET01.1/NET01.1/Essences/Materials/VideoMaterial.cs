﻿using System;
using NET01._1.Enums;
using NET01._1.Interfaces;

namespace NET01._1.Essences.Materials
{
    public class VideoMaterial : Material, IVersionable
    {
        private const int VersionLength = 8;

        private byte[] _version;
        public VideoFormat Format { get; set; }

        public byte[] Version
        {
            get { return _version; }
            set
            {
                if (value.Length > VersionLength)
                {
                    throw new Exception($"Version length can't be more than {VersionLength}");
                }
                else
                {
                    _version = value;
                }
            }
        }

        public VideoMaterial(VideoFormat format, byte[] version, string description) : base(description)
        {
            Version = version;
            Format = format;
        }

        private VideoMaterial()
        {
        }

        public override object Clone()
        {
            byte[] versionArr = new byte[this.Version.Length];

            for (int j = 0; j < this.Version.Length; j++)
            {
                versionArr[j] = this.Version[j];
            }

            return new VideoMaterial
            {
                Format = this.Format,
                Version = versionArr,
                Description = this.Description,
                Id = this.Id
            };
        }
    }
}
