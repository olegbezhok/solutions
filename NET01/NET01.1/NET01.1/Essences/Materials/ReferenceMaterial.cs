﻿using NET01._1.Enums;

namespace NET01._1.Essences.Materials
{
    public class ReferenceMaterial : Material
    {
        public ReferenceType Type { get; set; }

        public ReferenceMaterial(ReferenceType type, string description) : base(description)
        {
            Type = type;
        }

        private ReferenceMaterial()
        {

        }

        public override object Clone()
        {
            return new ReferenceMaterial
                {
                   Type = this.Type,
                    Description = this.Description,
                    Id = this.Id
                };
        }
    }
}
