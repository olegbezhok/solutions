﻿using System;

namespace NET01._1.Essences.Materials
{
    public abstract class Material : Entity, ICloneable
    {
        public Material(string description) : base(description)
        {
        }

        public Material()
        {
        }
        
        public abstract object Clone();
    }
}
