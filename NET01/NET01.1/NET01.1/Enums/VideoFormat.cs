﻿namespace NET01._1.Enums
{
    public enum VideoFormat
    {
        Unknown,
        Avi,
        Mp4,
        Flv
    }
}
