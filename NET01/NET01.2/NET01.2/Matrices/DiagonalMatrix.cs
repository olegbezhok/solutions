﻿using System;

namespace NET01._2.Matrices
{
    public class DiagonalMatrix<T> : SquareMatrix<T>
    {
        public DiagonalMatrix(int size) : base(size)
        {
        }

        protected override int GetValidIndex(int i, int j, bool isGet)
        {
            if (!isGet && i != j)
            {
                throw new ArgumentException("On diagonal matrix you can set value only on diagonal");
            }

            return i;
        }

        protected override int GetMatrixSize(int size)
        {
            return size;
        }
    }
}
