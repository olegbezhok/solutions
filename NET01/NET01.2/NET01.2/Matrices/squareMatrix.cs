﻿using System;
using NET01._2.Events;

namespace NET01._2.Matrices
{
    public class SquareMatrix<T>
    {
        protected T[] _matrixData;
        private const string invalidIndexEx = "index must be more or equals 0";
        private const string invalidIndexBorderEx = "index must be smaller than size";
        public readonly int _size;

        public event EventHandler<MatrixEventArgs<T>> ValueChanged;

        /// <summary>
        /// property for get and set value of a variable by indexes
        /// </summary>
        /// <param name="i">first index for square matrix</param>
        /// <param name="j">second index for square matrix</param>
        /// <returns>value of a variable from a quadratic matrix</returns>
        public T this[int i, int j]
        {
            get
            {
                IndexValidator(i, j);
                return _matrixData[GetValidIndex(i, j, true)];
            }
            set
            {
                IndexValidator(i, j);

                int index = GetValidIndex(i, j, false);
                T oldvalue = _matrixData[index];
                _matrixData[index] = value;

                ChangeValue(oldvalue, _matrixData[index], i, j);
            }
        }
        
        public SquareMatrix(int size)
        {
            if (size <= 0)
            {
                throw new ArgumentException("not valid size, size must be more than 0");
            }
            _size = size;

            var matrixSize = GetMatrixSize(size);
            _matrixData = new T[matrixSize];
        }

        /// <summary>
        /// checked validation for index of two-dimensional matrix
        /// </summary>
        /// <param name="i">first index for two-dimensional matrix</param>
        /// <param name="j">second index for two-dimensional matrix</param>
        /// <param name="size">size of two-dimensional matrix</param>
        private void IndexValidator(int i, int j)
        {
            if (i < 0 || j < 0)
            {
                throw new ArgumentException(invalidIndexEx);
            }
            else if (i >= _size || j >= _size)
            {
                throw  new ArgumentException(invalidIndexBorderEx);
            }
        }

        /// <summary>
        /// Event generation if old and new values are equals
        /// </summary>
        /// <param name="oldValue">the value to be changed</param>
        /// <param name="newValue">the value of which we change</param>
        private void ChangeValue(T oldValue, T newValue, int i, int j)
        {
            if (!Equals(oldValue, newValue))
            {
                ValueChanged?.Invoke(this, new MatrixEventArgs<T>(oldValue, newValue, i, j));
            }
        }

        protected virtual int GetValidIndex(int i, int j, bool isGet)
        {
            return i * _size + j;
        }

        protected virtual int GetMatrixSize(int size)
        {
            return size * size;
        }
    }
}
