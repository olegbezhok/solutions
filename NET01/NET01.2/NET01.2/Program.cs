﻿using System;
using NET01._2.Events;
using NET01._2.Matrices;

namespace NET01._2
{
    class Program
    {
        static void Main(string[] args)
        {
            SquareMatrix<int> matrix = new SquareMatrix<int>(2);
            DiagonalMatrix<int> matrixD = new DiagonalMatrix<int>(5);
            DiagonalMatrix<string> matrixS = new DiagonalMatrix<string>(2);

            matrixD.ValueChanged += ShowMessage;
            matrixS.ValueChanged += (object sender, MatrixEventArgs<string> e) => Console.WriteLine(e.Message);
            matrix.ValueChanged += delegate (object sender, MatrixEventArgs<int> e) { Console.WriteLine(e.Message); };

            matrixD[1, 1] = 2;
            //matrixD[1, 2] = 3;
            //matrixD[0, 1] = 0;
            matrixS[0, 0] = "asdafa";
            matrixS[0, 0] = null;
            matrixD[0, 0] = 2;
            matrixD[0, 0] = 2;
            matrixD[0, 0] = 5;
            matrix[1, 1] = 5;
            Console.WriteLine(matrix[1, 0] + " " + matrixD[1, 1]);
            Console.WriteLine(matrixS[0, 0] + " " + matrixD[0, 1]);
            matrix[-2, 1] = 4;
            Console.WriteLine($"matrix{matrix[10, 2]}");
            //matrix[1, 2] = 5;
            Console.Read();
        }

        public static void ShowMessage<T>(object sender, MatrixEventArgs<T> e)
        {
            Console.WriteLine(e.Message);
        }
    }
}
