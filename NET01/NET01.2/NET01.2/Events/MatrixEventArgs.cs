﻿using System;

namespace NET01._2.Events
{
    public class MatrixEventArgs<T> : EventArgs
    {
        public T OldData { get; }

        public T NewData { get; }

        public int[] Position { get; }

        public string Message { get; }

        public MatrixEventArgs(T oldData, T newData, int i, int j)
        {
            OldData = oldData;
            NewData = newData;
            Position = new[] {i, j};
            Message =
                $"value was changed from {oldData} to {newData} on position [{i},{j}]";
        }
    }
}
