﻿using System;
using System.Collections.Generic;
using System.Threading;
using ET03._1.BLL.Contracts.Observer;
using ET03._1.BLL.Contracts.States;
using NET03._1.Data.Contracts.Entities;
using NET03._1.Data.Contracts.Enums;

namespace ET03._1.BLL.Contracts.Models
{
    public class Sensor
    {
        private int _value;
        private IList<IObserver> _servers = new List<IObserver>();

        public Sensor()
        {
            Value = 0;
        }

        public Guid Id { get; set; }

        public string Name { get; set; }

        public SensorType Type { get; set; }

        public int Interval { get; set; }

        public State State { get; set; }

        public Timer Action { get; set; }

        public int Value
        {
            get => _value;
            set
            {
                if (_value != value)
                {
                    _value = value;
                    Notify();
                }
            }
        }

        public static SensorModel DTOtoEntityMapp(Sensor sensorDto)
        {
            return new SensorModel
            {
                Id = sensorDto.Id,
                Name = sensorDto.Name,
                Type = sensorDto.Type,
                Interval = sensorDto.Interval
            };
        }

        public static Sensor EntityToDtoMapp(SensorModel sensorDto)
        {
            return new Sensor
            {
                Id = sensorDto.Id,
                Name = sensorDto.Name,
                Type = sensorDto.Type,
                Interval = sensorDto.Interval
            };
        }

        public void Attach(IObserver investor)

        {

            _servers.Add(investor);

        }

        public void Detach(IObserver investor)

        {

            _servers.Remove(investor);

        }

        public void Notify()

        {

            foreach (var server in _servers)

            {

                server.Update(this);

            }

        }
    }
}
