﻿using ET03._1.BLL.Contracts.Models;

namespace ET03._1.BLL.Contracts.Observer
{
    public interface IObserver
    {
        void Update(Sensor sensor);
    }
}
