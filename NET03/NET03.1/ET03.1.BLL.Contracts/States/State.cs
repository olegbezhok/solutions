﻿using ET03._1.BLL.Contracts.Models;

namespace ET03._1.BLL.Contracts.States
{
    public abstract class State
    {
        public abstract void Handle(Sensor sensor);
        public abstract void StartWork(object sensor);
    }
}
