﻿using System.Collections.Generic;
using ET03._1.BLL.Contracts.Observer;

namespace ET03._1.BLL.Contracts.Services
{
    public interface ISensorService
    {
        void ChangeState(string sendorId);
        void DeleteSensor(string sendorId);
        List<string> GetSensorTypes();
        void AddSensor(string sensorName,string sensorType, int interval);
        List<string> GetAllSensors();
        void Attach(IObserver server);
    }
}
