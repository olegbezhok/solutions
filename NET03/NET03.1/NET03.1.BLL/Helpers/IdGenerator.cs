﻿using System;
using ET03._1.BLL.Contracts.Models;

namespace NET03._1.Data.Helpers
{
    public class IdGenerator
    {
        private static IdGenerator _instance;
        private static object syncRoot = new Object();

        private IdGenerator()
        {

        }

        public static IdGenerator CreateGenerator()
        {
            if (_instance == null)
            {
                lock (syncRoot)
                {
                    if (_instance == null)
                    {
                        return _instance ?? (_instance = new IdGenerator());
                    }
                }
            }

            return _instance;
        }

        public void GenerateId(Sensor sensor)
        {
            sensor.Id = Guid.NewGuid();
        }
    }
}
