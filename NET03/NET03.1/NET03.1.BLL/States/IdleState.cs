﻿using System.Threading;
using ET03._1.BLL.Contracts.Models;
using ET03._1.BLL.Contracts.States;

namespace NET03._1.BLL.States
{
    public class IdleState : State
    {
        private static IdleState _state;
        private static readonly object Locker = new object();

        private IdleState()
        {
        }

        public static IdleState CreateInstance()
        {
            lock (Locker)
            {
                return _state ?? (_state = new IdleState());
            }
        }

        public override void Handle(Sensor sensor)
        {
            sensor.State = CalibrationState.CreateInstance();
            
            sensor.Action = new Timer(sensor.State.StartWork, sensor, 0, sensor.Interval * 1000);
        }

        public override void StartWork(object sensor)
        {
            var ourSensor = (Sensor)sensor;
            ourSensor.Value = 0;
        }
    }
}
