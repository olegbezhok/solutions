﻿using System.Threading;
using ET03._1.BLL.Contracts.Models;
using ET03._1.BLL.Contracts.States;

namespace NET03._1.BLL.States
{
    public class CalibrationState: State
    {
        private static readonly object Locker = new object();
        private static CalibrationState _state;

        private CalibrationState()
        {
        }

        public static CalibrationState CreateInstance()
        {
            lock (Locker)
            {
                return _state ?? (_state = new CalibrationState());
            }
        }

        public override void Handle(Sensor sensor)
        {
            sensor.State = WorkState.CreateInstance();

            sensor.Action.Dispose();
            sensor.Action = new Timer(sensor.State.StartWork, sensor, 0, sensor.Interval * 1000);
        }

        public override void StartWork(object sensor)
        {
            var ourSensor = (Sensor) sensor;
            ourSensor.Value++;
        }
    }
}
