﻿using System;
using System.Threading;
using ET03._1.BLL.Contracts.Models;
using ET03._1.BLL.Contracts.States;

namespace NET03._1.BLL.States
{
    public class WorkState : State
    {
        private readonly Random _random = new Random();
        private static WorkState _state;
        private static readonly object Locker = new object();

        private WorkState()
        {
        }

        public static WorkState CreateInstance()
        {
            lock (Locker)
            {
                return _state ?? (_state = new WorkState());
            }
        }

        public override void Handle(Sensor sensor)
        {
            sensor.State = IdleState.CreateInstance();
            sensor.Action.Dispose();
            sensor.Action = new Timer(sensor.State.StartWork, sensor, 0, sensor.Interval * 1000);
        }

        public override void StartWork(object sensor)
        {
            var ourSensor = (Sensor) sensor;
            ourSensor.Value = _random.Next(500);
        }
    }
}
