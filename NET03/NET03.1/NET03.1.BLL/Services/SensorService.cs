﻿using System;
using System.Collections.Generic;
using System.Linq;
using ET03._1.BLL.Contracts.Models;
using ET03._1.BLL.Contracts.Observer;
using ET03._1.BLL.Contracts.Services;
using NET03._1.BLL.States;
using NET03._1.Data.Contracts.Enums;
using NET03._1.Data.Contracts.Factory;
using NET03._1.Data.Helpers;
using NET03._1.Data.Repositories;

namespace NET03._1.BLL.Services
{
    public class SensorService : ISensorService
    {
        private readonly IFileRepository _repository;
        private readonly IdGenerator _generator;
        private List<Sensor> _sensors = new List<Sensor>();

        public SensorService(string path)
        {
            var creator = new SensorCreator();
            _repository = creator.GetRepository(path);
            _generator = IdGenerator.CreateGenerator();
        }

        public List<string> GetSensorTypes()
        {
            return Enum.GetValues(typeof(SensorType))
                .Cast<SensorType>()
                .Select(v => v.ToString())
                .ToList(); 
        }

        public void ChangeState(string sensorId)
        {
            var sensor = _sensors.FirstOrDefault(x => x.Id== Guid.Parse(sensorId));
            if (sensor != null)
            {
                if (sensor.State == null)
                {
                    sensor.State = IdleState.CreateInstance();
                    sensor.State.StartWork(sensor);
                }

                else
                {
                    sensor.State.Handle(sensor);
                }
            }
        }

        public void DeleteSensor(string sensorId)
        {
            _repository.Delete(sensorId);
            _sensors.Remove(_sensors.First(x => x.Id == Guid.Parse(sensorId)));
        }

        public void ChangeSensor(Sensor sensor)
        {
            var sensorEntity = Sensor.DTOtoEntityMapp(sensor);

            _repository.Change(sensorEntity);
        }

        public void AddSensor(string sensorName,string sensorType, int interval)
        {
            var sensor = new Sensor
            {
                Interval = interval,
                Type = (SensorType)Enum.Parse(typeof(SensorType),sensorType),
                Name = sensorName
            };
            _generator.GenerateId(sensor);
            var sensorEntity = Sensor.DTOtoEntityMapp(sensor);
            _sensors.Add(sensor);
            _repository.Add(sensorEntity);
        }

        public List<string> GetAllSensors()
        {
            if (_sensors.Count == 0)
            {
                var sensors = _repository.GetAll();

                foreach (var sensor in sensors)
                {
                    _sensors.Add(Sensor.EntityToDtoMapp(sensor));
                }
            }
            var sensorNames = new List<string>();
            foreach (var sensor in _sensors)
            {
                sensorNames.Add(sensor.Id.ToString());
            }

            return sensorNames;
        }

        public void Attach(IObserver server)
        {
            foreach (var sensor in _sensors)
            {
                sensor.Attach(server);
            }
        }
    }
}
