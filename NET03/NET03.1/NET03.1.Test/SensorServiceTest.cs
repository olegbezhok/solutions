﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NET03._1.BLL.Services;
using NET03._1.Data.Contracts.Enums;

namespace NET03._1.Test
{
    [TestClass]
    public class SensorServiceTest
    {
        [TestMethod]
        public void GetSensorTypes()
        {
            //arrange
            var expected = new List<string>
            {
                "PressureSensor",
                "TemperatureSensor"
            };

            //act
            var actual = Enum.GetValues(typeof(SensorType))
                .Cast<SensorType>()
                .Select(v => v.ToString())
                .ToList();

            //assert
            CollectionAssert.AreEquivalent(expected, actual);
        }

        [TestMethod]
        public void GetAllSensors()
        {
            //arrange
            var expected = new List<string> {"alex"};

            var service = new SensorService(@"D:\Solutions\NET03\NET03.1\NET03.1\bin\Debug\Sensors.xml");

            //act
            var action = service.GetAllSensors();

            //assert
            CollectionAssert.AreEquivalent(expected, action);
        }

        [TestMethod]
        public void AddSensor()
        {
            //arrange
            var expected = new List<string> {"alex", "oleg"};
            var service = new SensorService(@"D:\Solutions\NET03\NET03.1\NET03.1\bin\Debug\Sensors.xml");

            //act
            service.GetAllSensors();
            service.AddSensor("oleg", "TemperatureSensor", 10);
            var action = service.GetAllSensors();

            //assert
            CollectionAssert.AreEquivalent(expected,action);
        }

        [TestMethod]
        public void DeleteSensor()
        {
            //arrange
            var expected = new List<string>{"alex"};
            var service = new SensorService(@"D:\Solutions\NET03\NET03.1\NET03.1\bin\Debug\Sensors.xml");

            //act
            service.GetAllSensors();
            service.DeleteSensor("oleg");
            var action = service.GetAllSensors();

            //assert
            CollectionAssert.AreEquivalent(expected, action);
        }
    }
}
