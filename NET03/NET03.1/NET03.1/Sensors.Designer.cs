﻿namespace NET03._1
{
    partial class Sensors
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.SensorIntervalValue = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.SenserTypeLabel = new System.Windows.Forms.Label();
            this.SensorTypesBox = new System.Windows.Forms.ComboBox();
            this.SensorNameLabel = new System.Windows.Forms.TextBox();
            this.SensorInformation = new System.Windows.Forms.TextBox();
            this.SensorsListBox = new System.Windows.Forms.ListBox();
            this.AddSensorButton = new System.Windows.Forms.Button();
            this.RemoveSensorButton = new System.Windows.Forms.Button();
            this.SwitchSensorButton = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SensorIntervalValue)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.SensorIntervalValue);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.SenserTypeLabel);
            this.panel1.Controls.Add(this.SensorTypesBox);
            this.panel1.Location = new System.Drawing.Point(12, 11);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(246, 106);
            this.panel1.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Sensor name:";
            // 
            // SensorIntervalValue
            // 
            this.SensorIntervalValue.Location = new System.Drawing.Point(90, 59);
            this.SensorIntervalValue.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.SensorIntervalValue.Name = "SensorIntervalValue";
            this.SensorIntervalValue.Size = new System.Drawing.Size(148, 20);
            this.SensorIntervalValue.TabIndex = 3;
            this.SensorIntervalValue.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Sensor Interval:";
            // 
            // SenserTypeLabel
            // 
            this.SenserTypeLabel.AutoSize = true;
            this.SenserTypeLabel.Location = new System.Drawing.Point(3, 17);
            this.SenserTypeLabel.Name = "SenserTypeLabel";
            this.SenserTypeLabel.Size = new System.Drawing.Size(70, 13);
            this.SenserTypeLabel.TabIndex = 1;
            this.SenserTypeLabel.Text = "Sensor Type:";
            // 
            // SensorTypesBox
            // 
            this.SensorTypesBox.FormattingEnabled = true;
            this.SensorTypesBox.Location = new System.Drawing.Point(90, 14);
            this.SensorTypesBox.Name = "SensorTypesBox";
            this.SensorTypesBox.Size = new System.Drawing.Size(148, 21);
            this.SensorTypesBox.TabIndex = 0;
            // 
            // SensorNameLabel
            // 
            this.SensorNameLabel.Location = new System.Drawing.Point(105, 50);
            this.SensorNameLabel.Multiline = true;
            this.SensorNameLabel.Name = "SensorNameLabel";
            this.SensorNameLabel.Size = new System.Drawing.Size(148, 21);
            this.SensorNameLabel.TabIndex = 1;
            this.SensorNameLabel.TextChanged += new System.EventHandler(this.SensorInformation_TextChanged);
            // 
            // SensorInformation
            // 
            this.SensorInformation.Location = new System.Drawing.Point(20, 229);
            this.SensorInformation.Multiline = true;
            this.SensorInformation.Name = "SensorInformation";
            this.SensorInformation.ReadOnly = true;
            this.SensorInformation.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.SensorInformation.Size = new System.Drawing.Size(673, 219);
            this.SensorInformation.TabIndex = 1;
            this.SensorInformation.TextChanged += new System.EventHandler(this.SensorInformation_TextChanged);
            // 
            // SensorsListBox
            // 
            this.SensorsListBox.FormattingEnabled = true;
            this.SensorsListBox.Location = new System.Drawing.Point(264, 11);
            this.SensorsListBox.Name = "SensorsListBox";
            this.SensorsListBox.Size = new System.Drawing.Size(429, 173);
            this.SensorsListBox.TabIndex = 2;
            // 
            // AddSensorButton
            // 
            this.AddSensorButton.Location = new System.Drawing.Point(139, 123);
            this.AddSensorButton.Name = "AddSensorButton";
            this.AddSensorButton.Size = new System.Drawing.Size(119, 31);
            this.AddSensorButton.TabIndex = 3;
            this.AddSensorButton.Text = "Add";
            this.AddSensorButton.UseVisualStyleBackColor = true;
            this.AddSensorButton.Click += new System.EventHandler(this.AddSensorButton_Click);
            // 
            // RemoveSensorButton
            // 
            this.RemoveSensorButton.Location = new System.Drawing.Point(322, 190);
            this.RemoveSensorButton.Name = "RemoveSensorButton";
            this.RemoveSensorButton.Size = new System.Drawing.Size(119, 31);
            this.RemoveSensorButton.TabIndex = 4;
            this.RemoveSensorButton.Text = "Remove";
            this.RemoveSensorButton.UseVisualStyleBackColor = true;
            this.RemoveSensorButton.Click += new System.EventHandler(this.RemoveSensorButton_Click);
            // 
            // SwitchSensorButton
            // 
            this.SwitchSensorButton.Location = new System.Drawing.Point(447, 190);
            this.SwitchSensorButton.Name = "SwitchSensorButton";
            this.SwitchSensorButton.Size = new System.Drawing.Size(246, 31);
            this.SwitchSensorButton.TabIndex = 5;
            this.SwitchSensorButton.Text = "Switch sensor state";
            this.SwitchSensorButton.UseVisualStyleBackColor = true;
            this.SwitchSensorButton.Click += new System.EventHandler(this.SwitchSensorButton_Click);
            // 
            // Sensors
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(705, 462);
            this.Controls.Add(this.SwitchSensorButton);
            this.Controls.Add(this.RemoveSensorButton);
            this.Controls.Add(this.AddSensorButton);
            this.Controls.Add(this.SensorsListBox);
            this.Controls.Add(this.SensorInformation);
            this.Controls.Add(this.SensorNameLabel);
            this.Controls.Add(this.panel1);
            this.Name = "Sensors";
            this.Text = "NET03.1";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SensorIntervalValue)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox SensorInformation;
        private System.Windows.Forms.ListBox SensorsListBox;
        private System.Windows.Forms.Button AddSensorButton;
        private System.Windows.Forms.Button RemoveSensorButton;
        private System.Windows.Forms.Button SwitchSensorButton;
        private System.Windows.Forms.NumericUpDown SensorIntervalValue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label SenserTypeLabel;
        private System.Windows.Forms.TextBox SensorNameLabel;
        private System.Windows.Forms.ComboBox SensorTypesBox;
        private System.Windows.Forms.Label label2;
    }
}

