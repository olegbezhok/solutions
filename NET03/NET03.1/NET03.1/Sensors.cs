﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Windows.Forms;
using ET03._1.BLL.Contracts.Models;
using ET03._1.BLL.Contracts.Observer;
using NET03._1.BLL.Services;

namespace NET03._1
{
    public partial class Sensors : Form, IObserver
    {
        private const string SectionName = "Repository";

        private readonly SensorService _sensorService =
            new SensorService(((Config)ConfigurationManager.GetSection(SectionName)).Path);

        public Sensors()
        {
            InitializeComponent();
            _sensorService.GetAllSensors();
            ReceiveInToControl(SensorsListBox, _sensorService.GetAllSensors());
            ReceiveInToControl(SensorTypesBox, _sensorService.GetSensorTypes());
            _sensorService.Attach(this);
        }

        public void Update(Sensor sensor)
        {
            var newText = $@"Sensor {sensor.Name}, Type: {sensor.Type}, value: {sensor.Value}" + "\r\n";
            if (SensorInformation.InvokeRequired)
            {
                SensorInformation.Invoke(new Action<string>(s => SensorInformation.Text += s), newText);
            }
            else
            {
                SensorInformation.Text += newText;
            }
        }

        private void SensorInformation_TextChanged(object sender, EventArgs e)
        {
            SensorInformation.SelectionStart = SensorInformation.Text.Length;
            SensorInformation.ScrollToCaret();
            SensorInformation.Refresh();
        }

        private static void ReceiveInToControl(ListControl listControl, ICollection<string> dataList)
        {
            if (dataList.Count <= 0)
            {
                return;
            }

            listControl.DataSource = dataList;
            listControl.SelectedIndex = 0;
        }

        private void RemoveSensorButton_Click(object sender, EventArgs e)
        {
            _sensorService.DeleteSensor(SensorsListBox.SelectedItem.ToString());
            ReceiveInToControl(SensorsListBox, _sensorService.GetAllSensors());
        }

        private void AddSensorButton_Click(object sender, EventArgs e)
        {
            _sensorService.AddSensor(SensorNameLabel.Text, SensorTypesBox.SelectedItem.ToString(), (int)SensorIntervalValue.Value);
            ReceiveInToControl(SensorsListBox, _sensorService.GetAllSensors());
        }

        private void SwitchSensorButton_Click(object sender, EventArgs e)
        {
            _sensorService.ChangeState(SensorsListBox.SelectedItem.ToString());
        }
    }
}
