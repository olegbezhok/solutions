﻿using System.Configuration;

namespace NET03._1
{
    public class Config : ConfigurationSection
    {
        [ConfigurationProperty("path")]
        public string Path
        {
            get => (string)this["path"];
            set => this["path"] = value;
        }
    }
}
