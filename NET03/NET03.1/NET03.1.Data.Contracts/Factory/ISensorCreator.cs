﻿using NET03._1.Data.Contracts.Factory;

namespace NET03._1.Data.Contracts.Repositories
{
    public interface ISensorCreator
    {
        IFileRepository GetRepository(string pathFile);
    }
}
