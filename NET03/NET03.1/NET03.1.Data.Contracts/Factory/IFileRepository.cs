﻿using System.Collections.Generic;
using NET03._1.Data.Contracts.Entities;

namespace NET03._1.Data.Contracts.Factory
{
    public interface IFileRepository
    {
        void Add(SensorModel sensor);
        IList<SensorModel> GetAll();
        void Delete(string name);
        void Change(SensorModel sensor);
    }
}
