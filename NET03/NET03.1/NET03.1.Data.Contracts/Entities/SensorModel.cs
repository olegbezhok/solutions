﻿using System;
using NET03._1.Data.Contracts.Enums;

namespace NET03._1.Data.Contracts.Entities
{
    public class SensorModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public SensorType Type { get; set; }

        public int Interval { get; set; }
    }
}
