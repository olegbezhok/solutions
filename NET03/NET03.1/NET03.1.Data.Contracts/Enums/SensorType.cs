﻿namespace NET03._1.Data.Contracts.Enums
{
    public enum SensorType
    {
        TemperatureSensor,
        PressureSensor
    }
}
