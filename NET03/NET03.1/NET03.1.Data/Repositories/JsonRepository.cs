﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using NET03._1.Data.Contracts.Entities;
using NET03._1.Data.Contracts.Factory;

namespace NET03._1.Data.Repositories
{
    public class JsonRepository : IFileRepository
    {
        private readonly string _path;
        private readonly JsonSerializer _serializer;

        public JsonRepository(string path)
        {
            _path = path;
            _serializer = new JsonSerializer();
        }

        public void Add(SensorModel sensor)
        {
            using (var file = File.CreateText(_path))
            {
                _serializer.Serialize(file, sensor);
            }
        }

        public IList<SensorModel> GetAll()
        {
            using (StreamReader reader = new StreamReader(_path))
            {
                string json = reader.ReadToEnd();
                return JsonConvert.DeserializeObject<List<SensorModel>>(json);
            }
        }

        public void Delete(string name)
        {
            List<SensorModel> sensors;
            using (StreamReader reader = new StreamReader(_path))
            {
                string json = reader.ReadToEnd();
                sensors = JsonConvert.DeserializeObject<List<SensorModel>>(json).ToList();
                sensors.Remove(sensors.FirstOrDefault(x => x.Name == name));
            }

            using (StreamWriter sw = new StreamWriter(_path))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                _serializer.Serialize(writer, sensors);
            }
        }

        public void Change(SensorModel sensor)
        {
            List<SensorModel> sensors;
            using (StreamReader reader = new StreamReader(_path))
            {
                string json = reader.ReadToEnd();
                sensors = JsonConvert.DeserializeObject<List<SensorModel>>(json).ToList();
                sensors.Remove(sensors.FirstOrDefault(x => x.Id == sensor.Id));
                sensors.Add(sensor);
            }

            using (StreamWriter sw = new StreamWriter(_path))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                _serializer.Serialize(writer, sensors);
            }
        }
    }
}
