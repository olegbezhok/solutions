﻿using NET03._1.Data.Contracts.Factory;
using NET03._1.Data.Contracts.Repositories;
using System;
using System.Text.RegularExpressions;

namespace NET03._1.Data.Repositories
{
    public class SensorCreator : ISensorCreator
    {
        private const string PathFileError = "The file path is incorrect.";
        private const string XmlPattern = @"\w*.xml$";
        private const string JsonPattern = @"\w*.json$";

        public IFileRepository GetRepository(string pathFile)
        {
            if (Regex.IsMatch(pathFile, XmlPattern, RegexOptions.IgnoreCase))
            {
                return new XmlRepository(pathFile);
            }

            if (Regex.IsMatch(pathFile, JsonPattern, RegexOptions.IgnoreCase))
            {
                return new JsonRepository(pathFile);
            }

            throw new ArgumentException(PathFileError);
        }
    }
}
