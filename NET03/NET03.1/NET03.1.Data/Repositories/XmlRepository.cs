﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using NET03._1.Data.Contracts.Entities;
using NET03._1.Data.Contracts.Enums;
using NET03._1.Data.Contracts.Factory;

namespace NET03._1.Data.Repositories
{
    public class XmlRepository : IFileRepository
    {
        private readonly string _path;
        private static XDocument _xDoc;

        public XmlRepository(string path)
        {
            _path = path;
            _xDoc = XDocument.Load(path);
        }

        public void Add(SensorModel sensor)
        {

            XElement root = _xDoc.Element("sensors");
            root.Add(new XElement("sensor",
                new XElement("id", sensor.Id),
                new XAttribute("name", sensor.Name),
                new XElement("type", sensor.Type.ToString()),
                new XElement("interval", sensor.Interval)));

            _xDoc.Save(_path);
        }

        public IList<SensorModel> GetAll()
        {
            return _xDoc.Element("sensors")
                ?.Elements("sensor").Select(x => new SensorModel
                {
                    Id = Guid.Parse(x.Element("id").Value),
                    Name = x.Attribute("name")?.Value,
                    Type = (SensorType)Enum.Parse(typeof(SensorType), x.Element("type")?.Value, true),
                    Interval = int.Parse(x.Element("interval")?.Value)
                }).ToList();
        }

        public void Delete(string name)
        {
            _xDoc.Element("sensors").Elements("sensor").Where(x => x.Attribute("name").Value == name).Remove();

            _xDoc.Save(_path);
        }

        public void Change(SensorModel sensor)
        {
           var result =  _xDoc.Element("sensors").Elements("sensor").FirstOrDefault(x => x.Element("id").Value == sensor.Id.ToString());

            result.Attribute("name").Value = sensor.Name;
            result.Element("type").Value = sensor.Type.ToString();
            result.Element("interval").Value = sensor.Interval.ToString();
        }
    }
}
