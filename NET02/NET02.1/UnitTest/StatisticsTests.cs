﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NET02._1.Entities;

namespace UnitTest
{
    [TestClass]
    public class StatisticsTests
    {
        [TestMethod]
        public void GetCountTestByName()
        {
            //arange
            var stat = CreateStatistics();

            var expectedNames = new List<string>
            {
                "Oleg Bezhok",
                "Sasha"
            };
            var expectedValues = new List<int>
            {
                2, 1
            };

            List<(string, int)> expected = new List<(string,int)>();
            (string,int) expect1 = ("Oleg Bezhok", 2);
            (string,int) expect2 = ("Sasha", 1);
            expected.Add(expect1);
            expected.Add(expect2);
            //act
            var actual = stat.GetCountTestByName();

            //assert
            CollectionAssert.AreEquivalent(expected, actual, "Pairs are not equivalent");
        }


        [TestMethod]
        public void FindMostPopularTest()
        {
            //arrange
            var stat = CreateStatistics();
            string expected = "bbb";

            //act
            var actual = stat.FindMostPopularTest();

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetTestWithLowestTrueAnswersCount()
        {
            //arrange
            var stat = CreateStatistics();
            string expexted = "bbb";

            //act
            var actual = stat.GetTestWithLowestTrueAnswersCount();

            //assert
            Assert.AreEqual(expexted, actual);
        }

        [TestMethod]
        public void GetCountRightAnswersByName()
        {
            //arrange
            var stat = CreateStatistics();

            var expectedTests = new List<string>
            {
                "ffff",
                "bbb"
            };

            (string, int) tuple1 = ("Oleg Bezhok", 3);
            (string, int) tuple2 = ("Oleg Bezhok", 2);
            (string, int) tuple3 = ("Sasha", 1);

            var expecdetValuesList = new List<(string, int)>();

            expecdetValuesList.Add(tuple1);
            expecdetValuesList.Add(tuple2);
            expecdetValuesList.Add(tuple3);

            ///act
            var actual = stat.GetCountRightAnswersByName();

            //assert
            CollectionAssert.AreEquivalent(expectedTests, actual.Keys.ToList());
        }

        private Statistics CreateStatistics()
        {
            Statistics stat = new Statistics();
            Answer answer1 = new Answer("ggggg", 1, true);
            Answer answer2 = new Answer("bbbb", 2, false);
            Answer answer3 = new Answer("cccc", 3, true);
            Answer answer4 = new Answer("dddd", 4, false);
            Answer answer5 = new Answer("eeee", 5, true);

            Test test1 = new Test(new List<Answer>
            {
                answer1,
                answer2,
                answer3,
                answer4,
                answer5
            }, "ffff", "Oleg Bezhok", DateTime.Now);

            Test tes3 = new Test(new List<Answer>()
            {
                answer1,
                answer2,
                answer4,
                answer5
            }, "bbb", "Bezhok, Oleg", DateTime.Now);

            Test test2 = new Test(new List<Answer>()
            {
                answer2,
                answer3
            }, "bbb", "Sasha", DateTime.Now);

            stat.Add(new List<Test> { test1, tes3, test2 });

            return stat;
        }
    }
}
