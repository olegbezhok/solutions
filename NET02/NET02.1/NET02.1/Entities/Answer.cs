﻿
namespace NET02._1.Entities
{
    public class Answer
    {
        public string FullText { get; }

        public int Сoefficient { get; }

        public bool IsTrueAnswer { get; }

        public Answer(string fullText, int coefficient, bool isTrue)
        {
            FullText = fullText;
            Сoefficient = coefficient;
            IsTrueAnswer = isTrue;
        }
    }
}
