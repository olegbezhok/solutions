﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace NET02._1.Entities
{
    public class Test : IEnumerable<Answer>
    {
        private List<Answer> _answers = new List<Answer>();

        public string TestName { get; }

        public string PersonName { get; set; }

        public DateTime PassageDate { get; }

        public int GetTrueAnswersCount()
        {
            return this.Where(x => x.IsTrueAnswer == true).Count();
        }

        public Test(List<Answer> answers, string testName, string personName, DateTime passageDate)
        {
            TestName = testName;
            PersonName = personName;
            PassageDate = passageDate;
            _answers = answers;
        }

        public IEnumerator<Answer> GetEnumerator()
        {
            foreach (var answer in _answers.OrderByDescending(x => x.Сoefficient))
            {
                yield return answer;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            foreach (var answer in this.OrderByDescending(x => x.Сoefficient))
            {
                yield return answer;
            }
        }
    }
}
