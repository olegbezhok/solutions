﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace NET02._1.Entities
{
    public class Statistics
    {
        private Dictionary<string, IList<Test>> _statDictionary = new Dictionary<string, IList<Test>>();

        public void Add(IList<Test> tests)
        {
            if (tests != null)
            {
                foreach (var test in tests)
                {
                    if (test.PersonName.Contains(","))
                    {
                        string[] nameArray = test.PersonName.Replace(" ", string.Empty).Split(',');
                        string name = nameArray[1] + " " + nameArray[0];
                        test.PersonName = name.ToString();
                    }

                    if (_statDictionary.ContainsKey(test.PersonName))
                    {
                        _statDictionary[test.PersonName].Add(test);
                    }
                    else
                    {
                        _statDictionary[test.PersonName] = new List<Test> { test };
                    }
                }
            }

            else
            {
                throw new ArgumentException("you can't add empty list of tests in statisctics");
            }
        }

        public List<(string,int)> GetCountTestByName()
        {
            return _statDictionary.Select(x => (x.Key, x.Value.Count())).ToList();
        }

        public string FindMostPopularTest()
        {
            return _statDictionary.SelectMany(s => s.Value).GroupBy(x => x.TestName)
                .OrderByDescending(x => x.Count()).FirstOrDefault().FirstOrDefault().TestName; ;
        }

        public Dictionary<string, List<(string, int)>> GetCountRightAnswersByName()
        {
            return _statDictionary.SelectMany(s => s.Value).GroupBy(x => x.TestName)
                .ToDictionary(x => x.Select(z => z.TestName)
                    .FirstOrDefault(), x => x.Select(z => (z.PersonName, z.GetTrueAnswersCount())).ToList());
        }

        public string GetTestWithLowestTrueAnswersCount()
        {
            return _statDictionary.SelectMany(s => s.Value).Distinct()
                .ToDictionary(k => k, k => k.GetTrueAnswersCount()).OrderBy(x => x.Value).FirstOrDefault().Key.TestName;
        }
    }
}


