﻿using NET02._1.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NET02._1
{
    class Program
    {
        static void Main(string[] args)
        {
            Statistics stat = new Statistics();
            Answer answer1 = new Answer("ggggg", 1, true);
            Answer answer2 = new Answer("bbbb", 2, false);
            Answer answer3 = new Answer("cccc", 3, true);
            Answer answer4 = new Answer("dddd", 4, false);
            Answer answer5 = new Answer("eeee", 5, true);

            Test test1 = new Test(new List<Answer>
            {
                answer1,
                answer2,
                answer3,
                answer4,
                answer5
            }, "ffff", "Oleg Bezhok", DateTime.Now);

            Test tes3 = new Test(new List<Answer>()
            {
                answer1,
                answer2,
                answer4,
                answer5
            }, "bbb", "Bezhok, Oleg", DateTime.Now);

            Test test2 = new Test(new List<Answer>()
            {
                answer2,
                answer3
            }, "bbb", "Sasha", DateTime.Now);

            stat.Add(new List<Test> { test1, tes3, test2 });
            var result = stat.GetCountTestByName();
            var result2 = stat.FindMostPopularTest();
            var result3 = stat.GetCountRightAnswersByName();
            var res = result3.Values;
            foreach (var answer in test1)
            {
                Console.WriteLine(answer.FullText);
            }
        }
    }
}
