﻿using System.Configuration;

namespace LoggerAssembly
{
    public class LoggerConfigReader : ConfigurationSection
    {
        [ConfigurationProperty("path")]
        public string Path
        {
            get => (string)this["path"];
            set => this["path"] = value;
        }
    }
}
