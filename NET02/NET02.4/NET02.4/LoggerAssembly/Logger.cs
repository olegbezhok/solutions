﻿using System.Configuration;
using System.IO;
using System.Text;

namespace LoggerAssembly
{
    public static class Logger
    {
        private static LoggerConfigReader _loggerConfigReader;

        static Logger()
        {

            _loggerConfigReader = (LoggerConfigReader)ConfigurationManager.GetSection("Logger");

        }

        public static void Log(string message)
        {
            var path = Path.GetFullPath($@"{_loggerConfigReader.Path}");
            using (FileStream fStream = new FileStream(path, FileMode.Append))
            {
                var messageArray = Encoding.Default.GetBytes("Information" + ": " + message + ";  ");
                fStream.Write(messageArray, 0, messageArray.Length);
                fStream.Flush();
            }
        }

    }
}
