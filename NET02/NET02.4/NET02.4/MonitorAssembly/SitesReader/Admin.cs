﻿using System.Configuration;

namespace MonitorAssembly.SitesReader
{
    public class Admin : ConfigurationElement
    {
        [ConfigurationProperty("mail")]
        public string Mail
        {
            get => (string)this["mail"];
            set => this["mail"] = value;
        }
    }
}
