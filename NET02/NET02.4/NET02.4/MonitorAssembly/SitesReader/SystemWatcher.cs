﻿using System.Configuration;

namespace MonitorAssembly.SitesReader
{
    public class SystemWatcher : ConfigurationElement
    {
        [ConfigurationProperty("file")]
        public string File {
            get => (string)this["file"];
            set => this["file"] = value;
        }
    }
}
