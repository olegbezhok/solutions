﻿using System.Configuration;

namespace MonitorAssembly.SitesReader
{
    [ConfigurationCollection(typeof(Site), AddItemName = "site")]
    public class SitesCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new Site();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((Site)(element)).GetType();
        }

        public Site this[int idx] => (Site)BaseGet(idx);
    }
}
