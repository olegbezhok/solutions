﻿using System.Configuration;

namespace MonitorAssembly.SitesReader
{
    public class Site : ConfigurationElement
    {
        [ConfigurationProperty("interval")]
        public int Interval
        {
            get => (int)this["interval"];
            set => this["interval"] = value;
        }

        [ConfigurationProperty("api")]
        public string Api
        {
            get => (string)this["api"];
            set => this["api"] = value;
        }

        [ConfigurationProperty("maxTimeAnswer")]
        public int MaxTimeAnswer
        {
            get => (int)this["maxTimeAnswer"];
            set => this["maxTimeAnswer"] = value;
        }

        [ConfigurationProperty("address")]
        public string Address
        {
            get => (string)this["address"];
            set => this["address"] = value;
        }

        [ConfigurationProperty("admin")]
        public Admin Admin
        {
            get => (Admin)this["admin"];
            set => this["admin"] = value;
        }
    }
}
