﻿using System.Configuration;

namespace MonitorAssembly.SitesReader
{
    public class SmtpServer : ConfigurationElement
    {
        [ConfigurationProperty("name")]
        public string Name
        {
            get => (string)this["name"];
            set => this["name"] = value;
        }

        [ConfigurationProperty("port")]
        public int Port
        {
            get => (int)this["port"];
            set => this["port"] = value;
        }
    }
}
