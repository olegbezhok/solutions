﻿using System.Configuration;

namespace MonitorAssembly.SitesReader
{
    public class Credentials : ConfigurationElement
    {
        [ConfigurationProperty("mail")]
        public string Mail
        {
            get => (string)this["mail"];
            set => this["mail"] = value;
        }
        [ConfigurationProperty("pass")]
        public string Pass
        {
            get => (string)this["pass"];
            set => this["pass"] = value;
        }
    }
}
