﻿using System.Configuration;

namespace MonitorAssembly.SitesReader
{
    public class MonitoringConfigReader : ConfigurationSection
    {
        [ConfigurationProperty("sites")]
        public SitesCollection SiteItems => (SitesCollection)base["sites"];

        [ConfigurationProperty("smtpServer")]
        public SmtpServer SmtpServer => (SmtpServer)this["smtpServer"];

        [ConfigurationProperty("credentials")]
        public Credentials Credentials => (Credentials)this["credentials"];

        [ConfigurationProperty("systemWatcher")]
        public SystemWatcher SystemWatcher => (SystemWatcher)this["systemWatcher"];
    }
}
