﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;
using LoggerAssembly;
using MonitorAssembly.SitesReader;

namespace MonitorAssembly
{
    public class Monitoring 
    {
        private static List<CancellationTokenSource> CancellationTokens = new List<CancellationTokenSource>();
        private static MonitoringConfigReader _monitoring;
        private static FileSystemWatcher _watcher;

        private static readonly object threadLock = new object();

        private Monitoring()
        {
            _watcher = new FileSystemWatcher()
            {
                NotifyFilter = NotifyFilters.LastWrite,
                Path = Environment.CurrentDirectory,
                Filter = _monitoring.SystemWatcher.File
            };
            _watcher.Changed += OnChanged;
            _watcher.EnableRaisingEvents = true;
        }

        public static Monitoring Monitor
        {
            get
            {
                lock (threadLock)
                {
                    if (_monitoring == null)
                    {
                        _monitoring = (MonitoringConfigReader)ConfigurationManager.GetSection("Monitor");

                    }
                    return new Monitoring();
                }
            }
        }
        
        public void StartCheck()
        {
            foreach (Site site in _monitoring.SiteItems)
            {
                var cancellationTokenSource = new CancellationTokenSource();
                var token = cancellationTokenSource.Token;
                CancellationTokens.Add(cancellationTokenSource);
                var firstCheck =
                    new Task(async () => await CheckSiteAsync(site, cancellationTokenSource, false), token);
                var secondCheck =
                    firstCheck.ContinueWith(task => CheckSiteAsync(site, cancellationTokenSource, false),
                        token);
                var thirdCheck =
                    secondCheck.ContinueWith(tasks => CheckSiteAsync(site, cancellationTokenSource, true),
                        token);

                firstCheck.Start();
            }
        }

        private async Task CheckSiteAsync(Site site, CancellationTokenSource cancellationTokenSource, bool isLastCheck)
        {
            using (var client = new HttpClient { Timeout = TimeSpan.FromSeconds(site.MaxTimeAnswer) })
            {
                cancellationTokenSource.Token.ThrowIfCancellationRequested();
                client.BaseAddress = new Uri(site.Address);
                using (var response = await client.GetAsync(site.Api))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        Logger.Log("success result for site check");
                        cancellationTokenSource.Cancel();
                    }
                    else if (isLastCheck && !response.IsSuccessStatusCode)
                    {
                        await SendEmail(site.Admin.Mail);
                    }

                    else
                    {
                        await Task.Delay(TimeSpan.FromSeconds(site.Interval));
                    }
                }
            }
        }

        private async Task SendEmail(string adminMail)
        {

            var from = new MailAddress(_monitoring.Credentials.Mail, "Oleg");
            var to = new MailAddress(adminMail);

            var mail = new MailMessage(from, to)
            {
                Subject = "Your site.",
                Body = "Hello. Can u check ur site, mb he is don't work."
            };

            using (var smtp = new SmtpClient(_monitoring.SmtpServer.Name, _monitoring.SmtpServer.Port))
            {
                smtp.Credentials = new NetworkCredential(_monitoring.Credentials.Mail, _monitoring.Credentials.Pass);
                smtp.EnableSsl = true;
                try
                {
                    await smtp.SendMailAsync(mail);
                }
                catch (SmtpException e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }
        }

        private void OnChanged(object source, FileSystemEventArgs e)
        {
            try
            {
                _watcher.EnableRaisingEvents = false;
                lock (CancellationTokens)
                {
                    foreach (var cts in CancellationTokens)
                    {
                        cts.Cancel();
                    }
                    lock (_monitoring)
                    {
                        ConfigurationManager.RefreshSection("Monitor");
                        _monitoring = (MonitoringConfigReader)ConfigurationManager.GetSection("Monitor");
                        StartCheck();
                    }
                }
            }

            finally
            {
                _watcher.EnableRaisingEvents = true;
            }
        }
    }
}
