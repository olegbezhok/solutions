﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using MonitorAssembly;

namespace NET02._4
{
    class Program
    {
        static void Main(string[] args)
        {
            bool existed;
            string guid = Marshal.GetTypeLibGuidForAssembly(Assembly.GetExecutingAssembly()).ToString();

            Mutex mutexObj = new Mutex(true, guid, out existed);

            if (existed)
            {
                Console.WriteLine("App work");
                Monitoring monitoring = Monitoring.Monitor;
                monitoring.StartCheck();
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("App already worked.");
                Thread.Sleep(3000);
                return;
            }
            Console.ReadLine();
        }
    }
}
