﻿namespace NET02._2.Entities
{
    class Configuration
    {
        private const int DefoltTop = 0;
        private const int DefoltLeft = 0;
        private const int DefoltWidth = 400;
        private const int DefoltHeight = 150;

        public string Title { get; set; }

        public int? Top { get; set; }

        public int? Left { get; set; }

        public int? Width { get; set; }

        public int? Height { get; set; }

        public static Configuration CreateDefoltConfig()
        {
            return new Configuration
            {
                Title = "main",
                Top = DefoltTop,
                Left = DefoltLeft,
                Height = DefoltHeight,
                Width = DefoltWidth
            };
        }

        public void ConfigValidate()
        {
            Top = Top.HasValue ? Top : DefoltTop;
            Left = Left.HasValue ? Left : DefoltLeft;
            Width = Width.HasValue ? Width : DefoltWidth;
            Height = Height.HasValue ? Height : DefoltHeight;
        }
    }
}
