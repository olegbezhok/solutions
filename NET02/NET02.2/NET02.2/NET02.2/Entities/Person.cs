﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace NET02._2.Entities
{
    class Person
    {
        public List<Configuration> Configurations { get; set; }

        public string Login { get; set; }

        public void ConfigValidator()
        {
            var mainConfig = Configurations.Where(x => x.Title == "main").FirstOrDefault();
            if (mainConfig == null)
            {
                Configurations.Add(Configuration.CreateDefoltConfig());
            }

            foreach (var config in Configurations)
            {
                config.ConfigValidate();
            }
        }
    }
}
