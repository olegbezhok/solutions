﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using NET02._2.Entities;
using System.IO;

namespace NET02._2.Reader
{
    public class XMLReader
    {
        private string _path;
        private List<Person> _persons;

        public XMLReader(string path)
        {
            _path = path;
            _persons = ReadData();
        }

        private List<Person> ReadData()
        {
            XDocument xDoc = XDocument.Load(_path + "Config.xml");
            return xDoc.Element("config").Elements("login").Select(x => new Person
            {
                Login = x.Attribute("name").Value,
                Configurations = x.Elements("window").Select(z => new Configuration
                {
                    Height = StringToNullableIntConverter(z.Element("left")?.Value),
                    Left = StringToNullableIntConverter(z.Element("left")?.Value),
                    Title = z.Attribute("title").Value,
                    Top = StringToNullableIntConverter(z.Element("lop")?.Value),
                    Width = StringToNullableIntConverter(z.Element("width")?.Value),
                }).ToList()
            }).ToList();
        }

        public string ShowData()
        {
            StringBuilder result = new StringBuilder();
            foreach (var person in _persons)
            {
                result.Append("Login: " + person.Login + "\n");
                foreach (var configuration in person.Configurations)
                {
                    result.Append(configuration.Title + "(" + configuration.Top + ", " + configuration.Left + ", " +
                                  +configuration.Width + ", " + configuration.Height + ")\n");
                }
            }

            return result.ToString();
        }

        public List<string> CheckIncorrectData()
        {
            return _persons.Where(x =>
                    x.Configurations.Count(z =>
                        z.Title == "main" && (z.Top == null || z.Left == null ||
                                                      z.Height == null ||
                                                      z.Width == null)) > 0 ||
                    x.Configurations.Count(z => z.Title == "main") == 0)
                .Select(a => a.Login).ToList();
        }

        public void JsonConverter()
        {
            foreach (var person in _persons)
            {
                using (StreamWriter file = File.CreateText(_path + person.Login))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    person.ConfigValidator();

                    serializer.Serialize(file, person);
                }
            }
        }

        private int? StringToNullableIntConverter(string value)
        {
            return value == null ? null : (int?)int.Parse(value);
        }
    }
}
