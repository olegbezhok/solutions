﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NET02._2.Reader;

namespace NET02._2.Test
{
    [TestClass]
    public class XMLReaderTest
    {
        [TestMethod]
        public void CheckIncorrectData()
        {
            //arrange 
            XMLReader reader = new XMLReader(@"D:\\Solutions\\NET02\\NET02.2\\NET02.2\\NET02.2\\Config\\");
            var expected = new List<string>
            {
                "user1",
                "user2"
            };

            //act
            var actual = reader.CheckIncorrectData();

            //assert
            CollectionAssert.AreEquivalent(expected, actual);
        }
    }
}
