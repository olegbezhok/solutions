﻿using System.Configuration;

namespace TextListenerAssembly
{
    public class TextListenerConfigReader : ConfigurationSection
    {
        [ConfigurationProperty("path")]
        public string Path
        {
            get => (string)this["path"];
            set => this["path"] = value;
        }
    }
}
