﻿using System.Configuration;
using System.IO;
using System.Text;
using IListenerAssembly;
using IListenerAssembly.Enums;

namespace TextListenerAssembly
{
    public class TextListener : IListener
    {
        public void Write(string message, LoggerLvl loggerLvl)
        {
            var reader = (TextListenerConfigReader)ConfigurationManager.GetSection("TextListener");

            using (FileStream fStream = new FileStream($@"{reader.Path}", FileMode.Append))
            {
                var messageArray = Encoding.Default.GetBytes(loggerLvl + ": " + message + ";  ");
                fStream.Write(messageArray, 0, messageArray.Length);
                fStream.Flush();
            }
        }
    }
}
