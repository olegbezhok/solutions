﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using AttributesAssembly;
using IListenerAssembly;
using IListenerAssembly.Enums;

namespace LoggerAssembly
{
    public static class Logger
    {
        private const int MaxCountForString = 4;
        private static List<IListener> _listeners = new List<IListener>();
        private const string EmptyLoggerEx = "Section Logger don't have any data.";
        private const string EmptyAssemblyEx = "Can't load our assembly. Assembly name is ";
        private const string LoggerLvlEx = "Logger lvl is not valid";
        private static LoggerLvl MinLvl = 0;

        static Logger()
        {

            var loggerConfigReader = (LoggerConfigReader)ConfigurationManager.GetSection("Logger");

            if (loggerConfigReader == null)
            {
                throw new ArgumentException(EmptyLoggerEx);
            }

            if (!Enum.TryParse(loggerConfigReader.MinLvl, out MinLvl))
            {
                throw new ArgumentException(LoggerLvlEx);
            }

            foreach (Element listener in loggerConfigReader.ListenerItems)
            {

                var assembly = Assembly.Load(listener.Assembly);

                if (assembly == null)
                {
                    throw new ArgumentException(EmptyAssemblyEx + listener.Assembly);
                }

                var listenerType = assembly.GetType(listener.Type);

                _listeners.Add((IListener)Activator.CreateInstance(listenerType));
            }
        }

        public static void Log(string message, LoggerLvl lvl)
        {
            foreach (var listener in _listeners)
            {
                if ((int)lvl >= (int)MinLvl)
                {
                    listener.Write(message, lvl);
                }
            }
        }

        public static void Track(object obj)
        {
            Type type = obj.GetType();
            if (type.GetCustomAttribute<TrackingEntityAttribute>() != null)
            {
                var propertyList = type.GetProperties().Where(x => x.GetCustomAttribute<TrackingPropertyAttribute>() != null).ToList();

                var fieldList = type.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic
                                               | BindingFlags.Static)
                    .Where(x => x.GetCustomAttribute<TrackingPropertyAttribute>() != null).ToList();

                if (propertyList.Count() != 0 || fieldList.Count() != 0)
                {
                    if (propertyList.Count() + fieldList.Count() > MaxCountForString)
                    {
                        StringBuilder messageBuilder = new StringBuilder();
                        foreach (PropertyInfo property in propertyList)
                        {
                            messageBuilder.Append((property.Name,
                                type.GetProperty(property.Name)?.GetValue(obj).ToString()).ToString());
                            messageBuilder.Append("; ");
                        }

                        foreach (FieldInfo field in fieldList)
                        {
                            messageBuilder.Append((field.Name, type.GetField(field.Name)?.GetValue(obj).ToString())
                                .ToString());
                            messageBuilder.Append("; ");
                        }

                        Log(messageBuilder.ToString(), LoggerLvl.Information);

                    }
                    else
                    {
                        string message = null;
                        foreach (PropertyInfo property in propertyList)
                        {
                            message += (property.Name, type.GetProperty(property.Name)?.GetValue(obj).ToString())
                                .ToString();
                            message += "; ";
                        }

                        foreach (FieldInfo field in fieldList)
                        {
                            message += (field.Name, type.GetFields(BindingFlags.Instance | BindingFlags.Public |
                                                                   BindingFlags.NonPublic
                                                                   | BindingFlags.Static)
                                .FirstOrDefault(x => x.Name == field.Name)
                                ?.GetValue(obj).ToString()).ToString();
                            message += "; ";
                        }

                        Log(message, LoggerLvl.Information);
                    }
                }
            }
        }
    }
}

