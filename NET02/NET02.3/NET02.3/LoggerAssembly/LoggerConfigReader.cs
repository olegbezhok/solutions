﻿using System.Configuration;

namespace LoggerAssembly
{
    public class LoggerConfigReader : ConfigurationSection
    {
        [ConfigurationProperty("listeners")]
        public ListenersCollection ListenerItems => (ListenersCollection)base["listeners"];

        [ConfigurationProperty("minLvl")]
        public string MinLvl
        {
            get => (string)this["minLvl"];
            set => this["minLvl"] = value;
        }
    }

    [ConfigurationCollection(typeof(Element), AddItemName = "listener")]
    public class ListenersCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new Element();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((Element)(element)).Type;
        }

        public Element this[int idx] => (Element)BaseGet(idx);
    }
    
    public class Element : ConfigurationElement
    {
        [ConfigurationProperty("assembly")]
        public string Assembly
        {
            get => (string)this["assembly"];
            set => this["assembly"] = value;
        }

        [ConfigurationProperty("type")]
        public string Type
        {
            get => (string)this["type"];
            set => this["type"] = value;
        }
    }
}
