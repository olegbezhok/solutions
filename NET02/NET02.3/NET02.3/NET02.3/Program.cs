﻿using IListenerAssembly.Enums;
using LoggerAssembly;
using NET02._3.Entities;

namespace NET02._3
{
    class Program
    {
        static void Main(string[] args)
        {
            Logger.Log("Some happened", LoggerLvl.Warning);
            Person person = new Person();
            person.Name = "Oleg";

            Logger.Track(person);
        }
    }
}
