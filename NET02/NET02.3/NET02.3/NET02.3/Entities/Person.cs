﻿using AttributesAssembly;

namespace NET02._3.Entities
{
    [TrackingEntity]
    public class Person
    {
        [TrackingProperty(Name = "Year")]
        private int Year = 2018;

        [TrackingProperty()]
        public string Name { get; set; }
    }
}
