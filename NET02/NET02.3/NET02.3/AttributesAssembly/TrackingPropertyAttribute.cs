﻿using System;

namespace AttributesAssembly
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public sealed class TrackingPropertyAttribute : Attribute
    {
        public string Name { get; set; }
    }
}
