﻿using System;

namespace AttributesAssembly
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
    public sealed class TrackingEntityAttribute : Attribute
    {
    }
}
