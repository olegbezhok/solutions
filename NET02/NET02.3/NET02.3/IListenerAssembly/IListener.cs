﻿using IListenerAssembly.Enums;

namespace IListenerAssembly
{
    public interface IListener
    {
        void Write(string message, LoggerLvl loggerLvl);
    }
}
