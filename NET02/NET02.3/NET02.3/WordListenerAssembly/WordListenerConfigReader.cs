﻿using System.Configuration;

namespace WordListenerAssembly
{
    public class WordListenerConfigReader : ConfigurationSection
    {
        [ConfigurationProperty("path")]
        public string Path
        {
            get => (string) this["path"];
            set => this["path"] = value;
        }
    }
}
