﻿using System.Configuration;
using System.IO;
using IListenerAssembly;
using IListenerAssembly.Enums;
using Microsoft.Office.Interop.Word;

namespace WordListenerAssembly
{
    public class WordListener : IListener
    {
        public void Write(string message, LoggerLvl loggerLvl)
        {
            var reader = (WordListenerConfigReader)ConfigurationManager.GetSection("WordListener");
            object missing = System.Reflection.Missing.Value;
            Application winWord = new Application
            {
                ShowAnimation = false,
                Visible = false
            };

            var document = File.Exists(reader.Path) ? winWord.Documents.Open(reader.Path) : winWord.Documents.Add(ref missing, ref missing, ref missing, ref missing);
            document.Content.Text += loggerLvl + ": " + message;
            SaveDocument(document, reader.Path, missing, winWord);
        }

        private static void SaveDocument(Document document, string path, object missing, Application winWord)
        {
            document.SaveAs2(path);
            document.Close(ref missing, ref missing, ref missing);
            winWord.Quit(ref missing, ref missing, ref missing);
        }
    }
}
