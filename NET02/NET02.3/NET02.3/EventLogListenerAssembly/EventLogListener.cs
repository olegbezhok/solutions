﻿using System.Configuration;
using System.Diagnostics;
using IListenerAssembly;
using IListenerAssembly.Enums;

namespace EventLogListenerAssembly
{
    public class EventLogListener : IListener
    {

        public void Write(string message, LoggerLvl loggerLvl)
        {
            var reader = (EventLogConfigReader)ConfigurationManager.GetSection("EventListener");

            using (EventLog eventLog = new EventLog(reader.Source))
            {
                eventLog.Source = reader.Source;
                if (loggerLvl.ToString() == "Error")
                {
                    eventLog.WriteEntry(message, EventLogEntryType.Error, 101, 1);
                }

                else if (loggerLvl.ToString() == "Warn")
                {
                    eventLog.WriteEntry(message, EventLogEntryType.Warning, 101, 1);
                }

                else
                {
                    eventLog.WriteEntry(message, EventLogEntryType.Information, 101, 1);
                }

            }
        }
    }
}
