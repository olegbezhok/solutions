﻿using System.Configuration;

namespace EventLogListenerAssembly
{
    public class EventLogConfigReader : ConfigurationSection
    {
        [ConfigurationProperty("source")]
        public string Source
        {
            get => (string)this["source"];
            set => this["source"] = value;
        }
    }
}
