﻿using System;
using System.Linq;
using WebProject.Data.Context;
using WebProject.Data.Contracts.Entities;
using WebProject.Data.Contracts.Entities.Enums;
using WebProject.Data.UnitOfWork;

namespace ConsoleTestRep
{
    class Program
    {
        static void Main(string[] args)
        {
            var unitOfWork = new UnitOfWork(new WebProjectContext());

            #region GetAllTest
            Console.WriteLine("GetAll test");
            foreach (var user in unitOfWork.UserRepository.GetAll())
            {
                Console.WriteLine(user.FullName + " Id: " + user.UserId);
            }
            #endregion

            #region GetByIdTest
            Console.WriteLine("\nGetById test");
            Guid id = new Guid("A2D70600-42D2-43D4-9328-09E07B3B0924");
            var testUser = unitOfWork.UserRepository.GetById(id);
            Console.WriteLine(testUser.FullName + " id: " + id + "\n");
            #endregion

            #region DelteTest
            Console.WriteLine("Delete test");
            unitOfWork.UserRepository.Delete(testUser);
            unitOfWork.Save();
            var deletedUsers = unitOfWork.UserRepository.GetAll().Where(x => x.IsDelete == true);
            foreach (var user in deletedUsers)
            {
                Console.WriteLine($"user with id = {user.UserId} has been removed");
            }
            #endregion

            #region UpdateTest
            Console.WriteLine("\nUpdate test");
            Console.WriteLine($"old email - {testUser.Email}");
            testUser.Email = "newEmail.com";
            testUser = unitOfWork.UserRepository.GetById(id);
            Console.WriteLine($"email after update - {testUser.Email}\n");
            #endregion

            #region AddTest
            Console.WriteLine("Add test");
            var test = new User
            {
                UserId = Guid.NewGuid(),
                DepartamentType = null,
                DepartamentTypeId = Guid.Parse("CEF960DE-F405-4187-BFD5-11C9E3DE73BD"),
                Email = "TestEmail@mail.ru",
                FullName = "Test Test",
                IsDelete = false,
                Location = "Test",
                Login = "Test",
                Pass = "Test",
                Photo = null,
                Role = (int) RoleType.ProjectManager,
                StartDate = DateTime.Now
            };
            unitOfWork.UserRepository.Add(test);
            unitOfWork.Save();
            Console.WriteLine($"New User : {unitOfWork.UserRepository.GetAll().FirstOrDefault(x=> x.Login == "Test").FullName}");
            #endregion

            Console.ReadLine();
        }
    }
}
