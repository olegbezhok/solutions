﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;
using WebProject.Services.Contracts.ControllerServices;
using WebProject.Services.Contracts.Model.DTOs;
using WebProject.Services.Contracts.Model.Enums;
using WebProject.Services.Contracts.Model.InputModels;
using WebProject.Services.Contracts.Model.ViewModels;
namespace WebProject.Controllers
{
    [RoutePrefix("Users")]
    public class UserController : BaseController
    {
        private IUserControllerService _userService;

        public UserController(IUserControllerService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        [Route()]
        [Authorize(Roles = nameof(RoleType.Admin))]
        public ActionResult Index()
        {
            var users = _userService.GetAll();
            return View(users);
        }

        [HttpGet]
        [Route("User/{id}")]
        [Authorize(Roles = nameof(RoleType.Admin))]
        public ActionResult GetUserById(string id)
        {
            try
            {
                var user = _userService.GetById(new Guid(id));
                return View(user);
            }
            catch (NullReferenceException)
            {
                return RedirectToAction("Error404", "Error");
            }
        }

        [HttpPost]
        [Route("Delete")]
        [Authorize(Roles = nameof(RoleType.Admin))]
        public ActionResult DeleteUser(string userId)
        {
            try
            {
                _userService.Delete(_userService.GetById(new Guid(userId)));
                return RedirectToAction("Index");
            }
            catch (NullReferenceException)
            {
                return RedirectToAction("Error404", "Error");
            }
        }

        [HttpGet]
        [Route("Add")]
        [Authorize(Roles = nameof(RoleType.Admin))]
        public ActionResult AddUser()
        {
            return View("AddUser");
        }

        [HttpPost]
        [Route("Add")]
        [Authorize(Roles = nameof(RoleType.Admin))]
        public Guid AddUser(UserModel user)
        {
            user.UserId = Guid.NewGuid();
            _userService.Add(AutoMapper.Mapper.Map<UserModel, UserDTO>(user));
            return user.UserId;
        }

        [HttpGet]
        [Route("Edit/{id}")]
        [Authorize(Roles = nameof(RoleType.Admin))]
        public ActionResult EditUser(string id)
        {
            try
            {
                var user = _userService.GetById(new Guid(id));
                return View(user);
            }
            catch (NullReferenceException)
            {
                return RedirectToAction("Error404", "Error");
            }
        }

        [HttpPost]
        [Route("Edit")]
        [Authorize(Roles = nameof(RoleType.Admin))]
        public ActionResult EditUser(UserModel user)
        {
            _userService.Update(AutoMapper.Mapper.Map<UserModel, UserDTO>(user));
            return View(AutoMapper.Mapper.Map<UserModel, UserDTO>(user));
        }

        [HttpPost]
        [Route("LoadPhoto")]
        [Authorize(Roles = nameof(RoleType.Admin))]
        public void LoadPhoto(FormCollection data)
        {
            var user = _userService.GetById(new Guid(data["userId"]));
            var value = Request.Files["photo"];
            if (Request.Files["photo"] != null)
            {
                using (var binaryReader = new BinaryReader(Request.Files["photo"].InputStream))
                {
                    var imagefile = binaryReader.ReadBytes(Request.Files["photo"].ContentLength);
                    user.Photo = imagefile;
                }
            }
            _userService.Update(user);
        }

        [Route("GetUserList")]
        [Authorize(Roles = nameof(RoleType.Admin))]
        public JsonResult GetUserList()
        {
            IList<UserShortInfoModel> usersShortInfo = new List<UserShortInfoModel>(AutoMapper.Mapper.Map<IEnumerable<UserDTO>, IEnumerable<UserShortInfoModel>>(_userService.GetAll().Where(x => x.IsDelete == false)));

            return Json(new { usersShortInfo }, JsonRequestBehavior.AllowGet);
        }

        [Route("Login")]
        public ActionResult LoginUser()
        {
            return View();
        }

        [HttpPost]
        [Route("Login")]
        public ActionResult LoginUser(LoginUserModel loginModel)
        {
            try
            {
                var user = _userService.Login(loginModel);

                CustomPrincipalSerializeModel serializeModel = new CustomPrincipalSerializeModel
                {
                    Id = user.UserId,
                    FullName = user.FullName,
                    Role = Enum.GetName(typeof(RoleType), user.Role)
                };

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var userData = serializer.Serialize(serializeModel);

                FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                    1,
                    user.Login,
                    DateTime.Now,
                    DateTime.Now.AddMinutes(15),
                    false,
                    userData);
                string encTicket = FormsAuthentication.Encrypt(authTicket);
                HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                Response.Cookies.Add(faCookie);

                return RedirectToAction("Index","Course");
            }
            catch (ArgumentNullException ex)
            {
                ModelState.AddModelError(ex.ParamName, ex.Message);

                return View(loginModel);
            }
        }

        [HttpGet]
        [Route("Logout")]
        [Authorize]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            
            HttpCookie cookie1 = new HttpCookie(FormsAuthentication.FormsCookieName, "");
            cookie1.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie1);
            
            SessionStateSection sessionStateSection = (SessionStateSection)WebConfigurationManager.GetSection("system.web/sessionState");
            HttpCookie cookie2 = new HttpCookie(sessionStateSection.CookieName, "");
            cookie2.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie2);

            return RedirectToAction("Index", "Course");
        }
    }
}