﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebProject.Services.Contracts.ControllerServices;
using WebProject.Services.Contracts.Model.DTOs;
using WebProject.Services.Contracts.Model.Enums;
using WebProject.Services.Contracts.Model.ViewModels;

namespace WebProject.Controllers
{
    [RoutePrefix("Courses")]
    public class CourseController : BaseController
    {
        private ICourseControllerService _courseService;

        public CourseController(ICourseControllerService courseService)
        {
            _courseService = courseService;
        }

        [HttpGet]
        [Route("~/")]
        public ActionResult Index()
        {
            var courses = _courseService.GetAll();
            return View(courses);
        }

        [HttpGet]
        [HandleError]
        [Route("Course/{courseId}")]
        public ActionResult GetById(Guid courseId)
        {
            try
            {
                var course = _courseService.GetById(courseId);
                if (User != null && User.IsInRole("Trainer"))
                {
                    foreach (var trainerCourseDto in course.TrainerCourses.Where(x => x.Trainer.UserId == User.Id))
                    {
                        trainerCourseDto.Course.IsCanEdit = true;
                    }
                }

                return View(course);
            }
            catch (NullReferenceException)
            {
                return RedirectToAction("Error404", "Error");
            }
        }

        [HttpGet]
        [Route("Edit/{courseId}")]
        [Authorize(Roles = nameof(RoleType.Admin) + "," + nameof(RoleType.Trainer))]
        public ActionResult EditCourse(Guid? courseId)
        {
            try
            {
                ViewBag.Trainers = AutoMapper.Mapper.Map<IEnumerable<TrainerDTO>, IEnumerable<TrainerShortInfo>>(_courseService.GetTrainerShortInfos());
                var course = _courseService.GetById(courseId.Value);
                ViewBag.CourseGroup = _courseService.GetCourseGroup();
                foreach (var trainerCourse in course.TrainerCourses)
                {
                    course.TrainerCourseGuids.Add(trainerCourse.TrainerId);
                }
                return View(course);
            }
            catch (NullReferenceException)
            {
                return RedirectToAction("Error404", "Error");
            }
        }

        [HttpPost]
        [Route("Edit/{courseId}")]
        [Authorize(Roles = nameof(RoleType.Admin) + "," + nameof(RoleType.Trainer))]
        public ActionResult EditCourse(CourseDTO course)
        {
            ViewBag.CourseGroup = _courseService.GetCourseGroup();
            ViewBag.Trainers = AutoMapper.Mapper.Map<IEnumerable<TrainerDTO>, IEnumerable<TrainerShortInfo>>(_courseService.GetTrainerShortInfos());

            CheckValidation(course, false);

            if (ModelState.IsValid)
            {
                _courseService.Update(course);
                return RedirectToAction("Index");
            }

            return View(course);
        }

        [HttpGet]
        [Route("Add")]
        [Authorize(Roles = nameof(RoleType.Admin))]
        public ActionResult AddCourse()
        {
            ViewBag.Trainers = AutoMapper.Mapper.Map<IEnumerable<TrainerDTO>, IEnumerable<TrainerShortInfo>>(_courseService.GetTrainerShortInfos());
            ViewBag.CourseGroup = _courseService.GetCourseGroup();
            return View("AddCourse");
        }

        [HttpPost]
        [Route("Add")]
        [Authorize(Roles = nameof(RoleType.Admin))]
        public ActionResult AddCourse(CourseDTO course)
        {
            CheckValidation(course, true);
            if (ModelState.IsValid)
            {
                course.CourseId = Guid.NewGuid();
                _courseService.Add(course);
                return RedirectToAction("Index");
            }
            ViewBag.CourseGroup = _courseService.GetCourseGroup();
            ViewBag.Trainers = AutoMapper.Mapper.Map<IEnumerable<TrainerDTO>, IEnumerable<TrainerShortInfo>>(_courseService.GetTrainerShortInfos());
            return View(course);
        }
        
        [Route("Delete/{courseId}")]
        [Authorize(Roles = nameof(RoleType.Admin))]
        public ActionResult DeleteCourse(Guid courseId)
        {
            try
            {
                _courseService.Delete(_courseService.GetById(courseId));
                return RedirectToAction("Index");
            }
            catch (NullReferenceException)
            {
                return RedirectToAction("Error404", "Error");
            }
        }

        public ActionResult MyCourses()
        {
            return View(_courseService.GetMyCourses(User.Id));
        }

        private void CheckValidation(CourseDTO course, bool isAdd)
        {
            if (string.IsNullOrEmpty(course.CourseName))
            {
                ModelState.AddModelError("CourseName", "Course Name is required");
            }

            if (isAdd == false && course.FormatedStartDate == null)
            {
                ModelState.AddModelError("FormatedStartDate", "Start Date is required");
            }

            if (isAdd== true && course.StartDate == null)
            {
                ModelState.AddModelError("StartDate", "Start Date is required");
            }

            if (course.TrainerCourseGuids.Count == 0)
            {
                ModelState.AddModelError("TrainerCourseGuids", "Choose trainer");
            }

            if (course.Description == null)
            {
                ModelState.AddModelError("Description", "Add description");
            }
        }
    }
}