﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using WebProject.Services.Contracts.ControllerServices;
using WebProject.Services.Contracts.Model.DTOs;
using WebProject.Services.Contracts.Model.Enums;
using WebProject.Services.Contracts.Model.InputModels;
using WebProject.Services.Contracts.Model.ViewModels;

namespace WebProject.Controllers
{
    [RoutePrefix("Trainers")]
    public class TrainerController : BaseController
    {
        private ITrainerControllerService _trainerService;

        public TrainerController(ITrainerControllerService trainerService)
        {
            _trainerService = trainerService;
        }

        [HttpGet]
        [Route()]
        [Authorize(Roles = nameof(RoleType.Admin))]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Route("Trainer/{id}")]
        [Authorize(Roles = nameof(RoleType.Admin))]
        public ActionResult GetTrainerById()
        {
                return View();
        }

        [HttpGet]
        [Route("Add")]
        [Authorize(Roles = nameof(RoleType.Admin))]
        public ActionResult AddTrainer()
        {
            return View();
        }

        [HttpGet]
        [Route("Edit/{id}")]
        [Authorize(Roles = nameof(RoleType.Admin) + "," + nameof(RoleType.Trainer))]
        public ActionResult EditTrainer()
        {
            return View();
        }

        [HttpGet]
        [Route("GetTrainerGroups")]
        [Authorize]
        public JsonResult GetTrainerGroups()
        {
            IList<TrainerGroupDTO> trainerGroups = new List<TrainerGroupDTO>(_trainerService.GetTrainerGroups());

            return Json(new { trainerGroups }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Route("HomePage")]
        [Authorize(Roles = nameof(RoleType.Trainer))]
        public ActionResult GetHomePage()
        {
            var trainer = _trainerService.GetTrainerByUserId(User.Id);
            return View(trainer);
        }
    }
}