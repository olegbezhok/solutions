﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using WebProject.Services.Contracts.ControllerServices;
using WebProject.Services.Contracts.Model.DTOs;
using WebProject.Services.Contracts.Model.InputModels;
using WebProject.Services.Contracts.Model.ViewModels;

namespace WebProject.Controllers
{
    public class TrainerApiController : ApiController
    {
        private ITrainerControllerService _trainerService;

        public TrainerApiController(ITrainerControllerService trainerService)
        {
            _trainerService = trainerService;
        }
        
        public IHttpActionResult GetTrainers()
        {
            var courses = _trainerService.GetAll();
            return Ok(courses);
        }

        [HttpGet]
        public IHttpActionResult GetById(Guid trainerId)
        {
            try
            {
                var trainer = _trainerService.GetById(trainerId);
                return Ok(trainer);
            }
            catch (NullReferenceException)
            {
                return BadRequest();
            }
        }

        [HttpDelete]
        public IHttpActionResult DeleteTrainer(Guid trainerId)
        {
            _trainerService.Delete(_trainerService.GetById(trainerId));
            return Ok();
        }

        [HttpPut]
        public IHttpActionResult AddTrainer(TrainerModel trainer)
        {
            trainer.TrainerId = Guid.NewGuid();
            _trainerService.Add(AutoMapper.Mapper.Map<TrainerModel, TrainerDTO>(trainer));

            return Ok(trainer);
        }

        [HttpPost]
        public IHttpActionResult EditTrainer(TrainerModel trainer)
        {
            var trainerModel = AutoMapper.Mapper.Map<TrainerModel, TrainerDTO>(trainer);
            _trainerService.Update(trainerModel);
            return Ok(trainer);
        }
    }
}
