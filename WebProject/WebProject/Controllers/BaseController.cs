﻿using System.Web.Mvc;
using WebProject.Services.CustomPrincipal;

namespace WebProject.Controllers
{
    public class BaseController : Controller
    {
        protected virtual new CustomPrincipal User
        {
            get { return HttpContext.User as CustomPrincipal; }
        }
    }
}