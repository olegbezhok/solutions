﻿using System.Web.Mvc;

namespace WebProject.Controllers
{
    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult Error404()
        {
            return View();
        }

        public ActionResult Unauthorised()
        {
            return View();
        }
    }
}