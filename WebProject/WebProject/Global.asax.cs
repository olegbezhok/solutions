﻿using System;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Script.Serialization;
using System.Web.Security;
using WebProject.Controllers;
using WebProject.Services.Contracts.Model.ViewModels;
using WebProject.Services.CustomPrincipal;

namespace WebProject
{
    public class MvcApplication : HttpApplication
    {
        private static SaveChangesHelper _helper;
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            App_Start.WebAutomapperConfig.Inicialize();

            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings
                .ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            GlobalConfiguration.Configuration.Formatters
                .Remove(GlobalConfiguration.Configuration.Formatters.XmlFormatter);

            //NinjectModule serviseModule = new ServiceModule();
            //NinjectModule webModule = new WebModule.WebModule();
            //var kernel = new StandardKernel(serviseModule, webModule);
            //kernel.Unbind<ModelValidatorProvider>();
            //DependencyResolver.SetResolver(new NinjectDependencyResolver(kernel));
        }

        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];

            if (authCookie != null)
            {
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);

                JavaScriptSerializer serializer = new JavaScriptSerializer();

                CustomPrincipalSerializeModel serializeModel = serializer.Deserialize<CustomPrincipalSerializeModel>(authTicket.UserData);

                CustomPrincipal newUser = new CustomPrincipal(authTicket.Name);
                newUser.Id = serializeModel.Id;
                newUser.FullName = serializeModel.FullName;
                newUser.Role = serializeModel.Role;

                HttpContext.Current.User = newUser;
            }
        }

        protected void Application_BeginRequest()
        {
            _helper = new SaveChangesHelper();
        }

        protected void Application_EndRequest()
        {
            if (Response.StatusCode == 401)
            {
                Response.ClearContent();
                Response.RedirectToRoute("ErrorHandler", (RouteTable.Routes["ErrorHandler"] as Route).Defaults);
            }

            if (Context.Response.StatusCode == 302 &&
                Context.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
            {
                Context.Response.StatusCode = 200;
            }

            if (Context.Response.StatusCode == 200 &&
                Context.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
            {
                _helper.SaveChanges();
            }
        }
    }
}
