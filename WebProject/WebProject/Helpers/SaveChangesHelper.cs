﻿using WebProject.Services.ServiceHelper;

namespace WebProject.Controllers
{
    public class SaveChangesHelper
    {
        private ServiceSaveChangesHelper _helper;

        public SaveChangesHelper()
        {
           _helper = new ServiceSaveChangesHelper();
        }

        public void SaveChanges()
        {
            _helper.SaveChanges();
        }
    }
}