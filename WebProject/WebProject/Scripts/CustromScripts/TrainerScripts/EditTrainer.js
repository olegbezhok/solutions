﻿var id;
var trainerGroups;

window.onload = function () {
    id = window.location.pathname.slice(-36);
    getTrainerGroups();
    loadTrainer();
    var confirm = document.getElementById('confirm');
    confirm.onclick = function (event) {
        checkValidation();
    }
};


function loadTrainer() {
    var obj = {
        trainerId: id
    }
    $.ajax({
        type: "GET",
        url: "/api/TrainerApi",
        data: obj,
        cache: false,
        success: function (data) {
            AddTrainerInView(data);
        }
    });
}

function AddTrainerInView(trainer) {
    addGroups(trainer.TrainerGroupId);
    $('#userId')[0].value = trainer.UserId;
    $('#trainerInfo')[0].value = trainer.TrainerInfo;
    $('#trainerId')[0].value = trainer.TrainerId;
    convertDate(trainer.FormatedStartDate);
    $('#endDate')[0].value = trainer.FormatedEndDate;
}

function checkValidation() {
    var i = 0;
    $('input').each(function () {
        if ($(this)[0].value == '' && $(this)[0].id != 'endDate') {
            $(this).removeClass("is-valid").addClass("is-invalid");
            i++;
        } else {
            $(this).removeClass("is-invalid").addClass("is-valid");
        }
    });
    if (i == 0) {
        editTrainer();
    }
}

function editTrainer() {
    var obj = {
        'UserId': $('#userId')[0].value,
        'TrainerId': $('#trainerId')[0].value,
        'TrainerInfo': $('#trainerInfo')[0].value,
        'TrainerGroupId': $('#trainerGroupSelect').children(":selected").attr("data-id"),
        'StartDate': $('#startDate')[0].value,
        'EndDate': $('#endDate')[0].value
    }
    $.ajax({
        type: "POST",
        url: "/api/TrainerApi",
        data: JSON.stringify(obj),
        contentType: "application/json; charset=utf-8",
        success: function () {
            changeLocation();
        }
    });
}

function changeLocation() {
    var isAdmin = document.getElementById('confirm').getAttribute("isadmin");
    if (!isAdmin) {
        window.location.href = "../";
    } else {
        window.location.href = "../HomePage"
    }
}

function convertDate(dateValue) {
    var date = new Date(dateValue);
    var day = ("0" + date.getDate()).slice(-2);
    var month = ("0" + (date.getMonth() + 1)).slice(-2);
    var value = date.getFullYear() + "-" + (month) + "-" + (day);
    $('#startDate').val(value);
}

function getTrainerGroups() {
    $.ajax({
        type: "GET",
        url: "../GetTrainerGroups",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            trainerGroups = data;
        }
    });
}

function addGroups(TrainerGroupId) {
    var trainerGroupSelect = $("#trainerGroupSelect");

    if (trainerGroupSelect[0].localName == 'input') {
        trainerGroupSelect[0].value = TrainerGroupId;
    }

    $.each(trainerGroups.trainerGroups, function (key, value) {
        if (value.TrainerGroupId == TrainerGroupId) {
            trainerGroupSelect.append(
                $('<option></option>').attr("value", value.Description).attr("data-id", value.TrainerGroupId).attr("selected", 'selected')
                    .text(value.Description));
        } else {
            trainerGroupSelect.append(
                $('<option></option>').attr("value", value.Description).attr("data-id", value.TrainerGroupId)
                    .text(value.Description));
        }
    });
}