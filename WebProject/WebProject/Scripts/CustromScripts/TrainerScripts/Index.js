﻿window.onload = function () {
    getTrainerGroups();
    var addBut = document.getElementById("addUser");

    addBut.onclick = function(event) {
        window.location.href = window.location.href + "/Add/";
    }
}

function LoadTrainers() {
    $.ajax({
        type: "GET",
        url: "/api/trainerApi",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        complete: function (data) {
            AddTrainers(data.responseJSON);
        }
    });
}

var trainerGroups;

function AddTrainers(data) {
    var trainerTable = $('#trainers');
    trainerTable[0].innerHTML = '';
    $.each(data,
        function (key, value) {
            var trainerGroupName
            $.each(trainerGroups.trainerGroups,
                function (key, group) {
                    if (group.TrainerGroupId == value.TrainerGroupId) {
                        trainerGroupName = group.Description
                    }
                });
            trainerTable.append("<tr><td>" +
                value.User.FullName +
                "</td><td>" +
                value.FormatedStartDate +
                "</td><td>" +
                value.FormatedEndDate +
                "</td><td>" +
                trainerGroupName +
                "</td><td>" +
                value.User.Email +
                "</td><td>" +
                "<button type='button' data-id=" +
                value.TrainerId +
                " class='btn btn-info' id='fullInfo'>Show Description</button>" +
                "</td></tr>");
        });
    var buttonInfo = document.getElementsByClassName("btn-info");
    $.each(buttonInfo,
        function(key, value) {
            value.onclick = function() {
                window.location.href = "/Trainers/Trainer/" + value.getAttribute("data-id");
            }
        });
}

function getTrainerGroups() {
    $.ajax({
        type: "GET",
        url: "/Trainers/GetTrainerGroups",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            trainerGroups = data;
            LoadTrainers()
        }
    });
}

