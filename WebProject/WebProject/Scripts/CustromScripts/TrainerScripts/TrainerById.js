﻿var id;
window.onload = function () {
    id = window.location.pathname.slice(-36);
    loadTrainer();
    var deleteBut = document.getElementById("delete");
    var editBut = document.getElementById("edit");
    var backBut = document.getElementById("goBack");

    backBut.onclick = function (event) {
        changeLocation();
    };

    deleteBut.onclick = function (event) {
        openSubmit(id);
    };

    editBut.onclick = function (event) {
        editUser(id);
    }
};
function loadTrainer() {
    var obj = {
        trainerId: id
    }
    $.ajax({
        type: "GET",
        url: "/api/TrainerApi",
        data: obj,
        cache: false,
        success: function (data) {
            AddTrainerInView(data);
        }
    });
}

function AddTrainerInView(trainer) {
    var info = document.getElementById("cardText");
    info.append("Email: " + trainer.User.Email);
    info.innerHTML = info.innerHTML + "<br />";
    info.append("Location: " + trainer.User.Email);
    info.innerHTML = info.innerHTML + "<br />";
    info.append("Start date: " + trainer.FormatedStartDate);
    info.innerHTML = info.innerHTML + "<br />";
    info.append("End date: " + trainer.FormatedEndDate);
    info.innerHTML = info.innerHTML + "<br />";
    info.append("Trainer info: " + trainer.TrainerInfo);
    info.innerHTML = info.innerHTML + "<br />";

    var title = document.getElementById("title");
    title.append(trainer.User.FullName);
    
    $('#ItemPreview').attr('src', `data:image/png;base64,${trainer.User.Photo}`);
}

function editUser(id) {
    var obj = {
        id: id
    }
    $.ajax({
        type: "GET",
        url: "../Edit",
        data: obj,
        cache: false,
        success:
            window.location.href = "../Edit/" + id
    });
}

function openSubmit(id) {
    var result = confirm("Are u sure?");
    if (result) {
        var obj = {
            trainerId: id
        }

        $.ajax({
            type: "DELETE",
            url: "/api/TrainerApi?trainerId=" + id,
            data: obj,
            cache: false,
            success: function () {
                changeLocation();
            }
        });
    }
}

function changeLocation() {
    $.ajax({
        type: "GET",
        url: "../",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: window.location.href = "../"
    });
}
