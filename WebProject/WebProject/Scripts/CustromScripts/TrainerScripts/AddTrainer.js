﻿window.onload = function () {
    loadUsers();
    getTrainerGroups();
    var confirm = document.getElementById('confirm');

    confirm.onclick = function (event) {
        event.stopPropagation();
        checkValidation();
    }
}

function checkValidation() {
    var i = 0;
    $('input').each(function () {
        if ($(this)[0].value == '') {
            $(this).removeClass("is-valid").addClass("is-invalid");
            i++;
        } else {
            $(this).removeClass("is-invalid").addClass("is-valid");
        }
    });
    if (i == 0) {
        addTrainer();
    }
}

function addTrainer() {
    var obj = {
        'UserId': $('#userSelect').children(":selected").attr("data-id"),
        'TrainerInfo': $('#trainerInfo')[0].value,
        'TrainerGroupId': $('#trainerGroupSelect').children(":selected").attr("data-id"),
        'StartDate': $('#startDate')[0].value
    }
    $.ajax({
        type: "PUT",
        url: "/api/TrainerApi",
        data: JSON.stringify(obj),
        contentType: "application/json; charset=utf-8",
        success: function () {
            changeLocation();
        }
    });
}

function changeLocation() {
    $.ajax({
        type: "GET",
        url: "/Trainers",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        complete: window.location.href = "/Trainers"
    });
}


function loadUsers() {
    $.ajax({
        type: "GET",
        url: "/Users/GetUserList",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            addUserList(data.usersShortInfo)
        }
    });
}

function addUserList(users) {
    var trainerGroupSelect = $("#userSelect");

    $.each(users,
        function (key, value) {
            trainerGroupSelect.append(
                $('<option></option>').attr("value", value.FullName).attr("data-id", value.UserId)
                    .text(value.FullName));
        });
}

function getTrainerGroups() {
    $.ajax({
        type: "GET",
        url: "../GetTrainerGroups",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            addGroups(data.trainerGroups)
        }
    });
}

function addGroups(groups) {
    var trainerGroupSelect = $("#trainerGroupSelect");

    $.each(groups, function (key, value) {
        trainerGroupSelect.append(
            $('<option></option>').attr("value", value.Description).attr("data-id", value.TrainerGroupId)
                .text(value.Description));
    });
}