﻿window.onload = function () {
    var deleteBut = document.getElementById("delete");
    var editBut = document.getElementById("edit");
    deleteBut.onclick = function (event) {
        var id = deleteBut.getAttribute('data-id');
        openSubmit(id);
    };

    editBut.onclick = function (event) {
        var id = editBut.getAttribute('data-id')
        editUser(id);
    }
};

function editUser(id) {
    var obj = {
        id: id
    }
    $.ajax({
        type: "GET",
        url: "../Edit",
        data: obj,
        cache: false,
        success:
            window.location.href = "../Edit/" + id
    });
}

function openSubmit(id) {
    var result = confirm("Are u sure?");
    if (result) {
        var obj = {
            userId: id
        }
        $.ajax({
            type: "POST",
            url: "../Delete",
            data: JSON.stringify(obj),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success:
                changeLocation()
        });
    }
}

function changeLocation() {
    $.ajax({
        type: "GET",
        url: "../",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: window.location.href = "../"
    });
}
