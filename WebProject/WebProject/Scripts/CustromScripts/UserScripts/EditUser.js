﻿window.onload = function () {
    var roleSelect = $('#roleSelect');
    var departSelect = $('#departSelect');
    addRoles(roleSelect);
    addDepart(departSelect);
    var confirm = document.getElementById('confirm');
    convertDate();
    confirm.onclick = function (event) {
        checkValidation();
    }
};

function convertDate() {
    var dateValue = document.getElementById('startDate').getAttribute('value-data');
    var date = new Date(dateValue);
    var day = ("0" + date.getDate()).slice(-2);
    var month = ("0" + (date.getMonth() + 1)).slice(-2);
    var value = date.getFullYear() + "-" + (month) + "-" + (day);
    $('#startDate').val(value);
}

function addRoles(roleSelect) {
    var roleId = document.getElementById("roleSelect").getAttribute("value-id");
    $.each(Roles, function (key, value) {
        if (key == roleId) {
            roleSelect.append(
                $('<option></option>').attr("value", value).attr("data-id", key).attr("selected", 'selected')
                    .text(value));
        } else {
            roleSelect.append(
                $('<option></option>').attr("value", value).attr("data-id", key)
                    .text(value));
        }
    });
}

function addDepart(departSelect) {
    var depatId = document.getElementById("departSelect").getAttribute("value-id");
    $.each(DepartamentTypes, function (key, value) {
        if (key == depatId) {
            departSelect.append(
                $('<option></option>').attr("value", value).attr("data-id", key).attr("selected", 'selected')
                    .text(value));
        } else {
            departSelect.append(
                $('<option></option>').attr("value", value).attr("data-id", key)
                    .text(value));
        }
    });
}

function checkValidation() {
    var i = 0;
    $('input').each(function () {
        var value = $(this)[0].value;
        var id = $(this)[0].id;
        if ($(this)[0].value == '' && $(this)[0].id != 'photo' && $(this)[0].id != 'delete') {
            $(this).removeClass("is-valid").addClass("is-invalid");
            i++;
        } else {
            if ($(this)[0].id == 'photo' && $(this)[0].value != '') {
                var format = $(this)[0].value.substr($(this)[0].value.length - 4);
                if (format == '.gif' || format == 'jpeg' || format == ".png" || format == ".jpg") {
                    $(this).removeClass("is-invalid").addClass("is-valid");
                } else {
                    $(this).removeClass("is-valid").addClass("is-invalid");
                }
            } else {
                $(this).removeClass("is-invalid").addClass("is-valid");
            }
        }
    });
    if (i == 0) {
        editUser();
    }

}


function editUser() {
    var value = document.getElementById('photo').getAttribute('value');
    var obj = {
        'Photo': value,
        'UserId': $('#userId')[0].value,
        'Role': $('#roleSelect').children(":selected").attr("data-id"),
        'FullName': $('#fullName')[0].value,
        'Email': $('#email')[0].value,
        'DepartamentTypeId': $('#departSelect').children(":selected").attr("data-id"),
        'Location': $('#location')[0].value,
        'StartDate': $('#startDate')[0].value,
        'Login': $('#login')[0].value,
        'Pass': $('#pass')[0].value,
        'IsDelete': $('#delete')[0].value,
    }
    $.ajax({
        type: "POST",
        url: "../Edit",
        data: JSON.stringify(obj),
        contentType: "application/json; charset=utf-8",
        success: function() {
            loadPhoto($('#userId')[0].value)
        }
    });
}

function loadPhoto(id) {
    var fd = new FormData();
    var file = document.getElementById('photo').files[0];
    fd.append("userId", id);
    fd.append("photo", file);
    if (file) {
        var xhr = new XMLHttpRequest();
        xhr.open("POST", "../LoadPhoto", false);
        xhr.send(fd);
        xhr.complete = changeLocation();
    } else {
        changeLocation();
    }
}


function changeLocation() {
    $.ajax({
        type: "GET",
        url: "../",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        complete: window.location.href = "../"
    });
}

