﻿window.onload = function () {
    var roleSelect = $('#roleSelect');
    var departSelect = $('#departamentSelect');
    var confirm = document.getElementById('confirm');
    var cancel = document.getElementById('cancel');
    cancel.onclick = function (event) {
        changeLocation();
    };
    addRoles(roleSelect);
    addDepart(departSelect);
    confirm.onclick = function (event) {
        checkValidation()
    }

};

function addRoles(roleSelect) {
    $.each(Roles, function (key, value) {
        roleSelect.append(
            $('<option></option>').attr("value", value).attr("data-id", key)
                .text(value));
    });
}

function addDepart(departSelect) {
    $.each(DepartamentTypes, function (key, value) {
        departSelect.append(
            $('<option></option>').attr("value", value).attr("data-id", key)
                .text(value));
    });
}

function checkValidation() {
    var i = 0;
    $('input').each(function () {
        if ($(this)[0].value == '') {
            $(this).removeClass("is-valid").addClass("is-invalid");
            i++;
        } else {
            if ($(this)[0].id == 'photo') {
                var format = $(this)[0].value.substr($(this)[0].value.length - 4);
                if (format == '.gif' || format == 'jpeg' || format == ".png" || format == ".jpg") {
                    $(this).removeClass("is-invalid").addClass("is-valid");
                } else {
                    $(this).removeClass("is-valid").addClass("is-invalid");
                }
            } else {
                $(this).removeClass("is-invalid").addClass("is-valid");
            }
        }
    });
    if (i == 0) {
        AddUser();
    }

}

function AddUser() {
    var obj = {
        'Role': $('#roleSelect').children(":selected").attr("data-id"),
        'FullName': $('#fullName')[0].value,
        'Email': $('#email')[0].value,
        'DepartamentTypeId': $('#departamentSelect').children(":selected").attr("data-id"),
        'StartDate': $('#startDate')[0].value,
        'Login': $('#login')[0].value,
        'Pass': $('#pass')[0].value,
        'Location': $('#location')[0].value
    }

    $.ajax({
        type: "POST",
        url: "./Add",
        data: JSON.stringify(obj),
        contentType: "application/json; charset=utf-8",
        success: function (data) {
                loadPhoto(data);
            }
    });
}

function loadPhoto(id) {
    var fd = new FormData();
    var file = document.getElementById('photo').files[0];
    fd.append("userId", id);
    fd.append("photo", file);

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "./LoadPhoto", false);
    xhr.send(fd);
    xhr.complete = changeLocation();
}


function changeLocation() {
    $.ajax({
        type: "GET",
        url: "./",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        complete: window.location.href = "./"
    });
}