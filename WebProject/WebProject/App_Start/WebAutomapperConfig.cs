﻿using AutoMapper;
using WebProject.Mappers;
using WebProject.Services.Mappers;

namespace WebProject.App_Start
{
    public class WebAutomapperConfig
    {
        public static void Inicialize()
        {
            Mapper.Initialize(
                x =>
                {
                    ServiceMapperConfig.ConfigAction.Invoke(x);
                    WebProjectMapperConfig.ConfigAction.Invoke(x);
                });
        }
    }
}