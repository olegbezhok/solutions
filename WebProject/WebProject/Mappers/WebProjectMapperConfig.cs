﻿using AutoMapper;
using System;
using WebProject.Mappers.Profiles;

namespace WebProject.Mappers
{
    public class WebProjectMapperConfig
    {
        public static Action<IMapperConfigurationExpression> ConfigAction = cfg =>
        {
            cfg.Advanced.AllowAdditiveTypeMapCreation = true;
            cfg.AddProfile<UserProfile>();
            cfg.AddProfile<TrainerProfile>();
        };
    }
}