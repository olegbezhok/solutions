﻿using AutoMapper;
using WebProject.Services.Contracts.Model.DTOs;
using WebProject.Services.Contracts.Model.InputModels;
using WebProject.Services.Contracts.Model.ViewModels;

namespace WebProject.Mappers.Profiles
{
    public class TrainerProfile : Profile
    {
        public TrainerProfile()
        {
            this.CreateMap<TrainerDTO, TrainerModel>().ReverseMap();
            this.CreateMap<TrainerDTO, TrainerViewModel>()
                .ForMember(dest => dest.User, opt => opt.MapFrom(src => src.User))
                .ReverseMap();
            this.CreateMap<TrainerDTO, TrainerShortInfo>().ForMember(src=> src.FullName, opt => opt.MapFrom(c=> c.User.FullName));
        }
    }
}