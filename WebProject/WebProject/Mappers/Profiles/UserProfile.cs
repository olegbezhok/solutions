﻿using AutoMapper;
using WebProject.Services.Contracts.Model.DTOs;
using WebProject.Services.Contracts.Model.InputModels;
using WebProject.Services.Contracts.Model.ViewModels;
using WebProject.Services.Mappers;

namespace WebProject.Mappers.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            this.CreateMap<UserModel, UserDTO>().Ignore(src => src.Trainers);
            this.CreateMap<UserDTO, UserViewModel>().ReverseMap();
            this.CreateMap<UserDTO, UserShortInfoModel>();
        }
    }
}