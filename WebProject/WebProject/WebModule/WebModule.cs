﻿using Ninject.Modules;
using WebProject.Services.Contracts.ControllerServices;
using WebProject.Services.ControllerServices;

namespace WebProject.WebModule
{
    public class WebModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUserControllerService>().To<UserControllerService>();
            Bind<ITrainerControllerService>().To<TrainerControllerService>();
            Bind<ICourseControllerService>().To<CourseControllerService>();
        }
    }
}