﻿MERGE dbo.Courses
USING (
 VALUES
	('C34AC5B5-5242-4E1C-B214-87B106FFBC84', N'Java', 1, N'На тренинге изучаются фреймворки и технологии Java, использующиеся для веб-разработки (back end
	и front end). В тренинг входят в виде отдельных компонентов тренинги «Front End Development for
	.NET and Java Developers» и «Database Development for .NET and Java Developers». В процессе тренинга
	выполняется разработка учебного проекта, приближенного к реальному по организации и процессам.
	Целевой аудиторией являются сотрудники, владеющие основами веб-разработки на Java.', '2014-02-20', '75DB2130-2482-43FD-B11C-603A38058416'),
	('691BCC81-242A-4799-AF55-C1FB5EE1F259', N'.NET Framework Fundamentals', 2, N'На тренинге рассматриваются следующие темы: жизненный цикл объектов; классы для
	представления коллекций; технология LINQ to Objects; реактивные расширения; файлы и потоки
	данных; сборки и метаданные; кодогенерация. Тренинг рассчитан на специалистов, желающих
	получить или систематизировать знания по базовым технологиям платформы .NET.','2017-11-14', '82F8D05E-77F8-453E-BEDE-12FA7BCA586F'),
	('98AFBFD1-8FC9-43E9-AB63-CD784629E1FC', N'.NET WEX Probation', 1,N'Целью тренинга является подготовка программистов для работы на проекте Wex Health 1Cloud.
	Тренинг подразумевает создание программного обеспечения по сформулированным требованиям и
	макетам с применением технологий, используемых на проекте Wex Health 1Cloud. Тренинг знакомит
	с предметной областью проекта Wex Health 1Cloud и базовыми процессами, протекающими в
	системе. Тренинг рассчитан на .NET-специалистов.', '2017-10-23', '82F8D05E-77F8-453E-BEDE-12FA7BCA586F'),
	('4D656CC0-CECE-4630-9749-347665987397', N'Angular 2.0', 1, ' ', '2017-12-14', '1600F8EA-D047-45FC-9DE8-889AB4D5F429')
) items (Id, CourseName, CourseTypeId, [Description], StartDate, CourseGroupId) ON dbo.Courses.CourseId = items.[Id]
WHEN NOT MATCHED BY TARGET THEN
 INSERT (CourseId, CourseName, CourseTypeId, [Description], StartDate, CourseGroupId)
 VALUES (items.Id, items.CourseName, items.CourseTypeId, items.[Description], items.StartDate, items.CourseGroupId);
