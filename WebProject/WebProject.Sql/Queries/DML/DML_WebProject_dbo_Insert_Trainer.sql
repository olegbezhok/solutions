﻿ INSERT INTO dbo.Trainers(TrainerId, UserId, TrainerInfo, TrainerGroupId, StartDate)
 VALUES
 	('314B08A7-44F5-4FB0-8D67-0A56C37C3CE2','A2D70600-42D2-43D4-9328-09E07B3B0924', 
		N'Разработчик программного обеспечения с опытом работы более семи лет. 
		В компании выступает в роли разработчика, лидера команды. 
		Проводит практические и лекционные тренинги по Angular и Front-End-разработке. 
		Любит общение с инициативными, интересными людьми.', '49B25EE2-BF90-42A6-A17A-C5571595E259', '2014-07-21'),
	('CEA05D16-BCBA-4480-AC0C-F75612D739B4','231A6BB3-D463-4579-916E-F9498BADB0D2', 
		N'Разработчик программного обеспечения с опытом работы более тринадцати лет.
		В компании проводит практические тренинги по .NET Framework.
		Окончил ММФ БГУ. Увлекается сноубордингом.', '5B1B834B-33F9-4298-AC34-539C8A5C64E1', '2017-10-23'),
	('F9D6ABDC-8F79-4660-A1B8-CA202DFFED1F','2BCE4E12-E021-4CAC-B217-1DDC61E83DB4', 
		N'Разработчик программного обеспечения и преподаватель с опытом работы более двадцати лет. 
		Проводит практические и лекционные тренинги по платформе .NET, ASP.NET Core, языку Python, 
		Front-End разработке, алгоритмам и структурам данных, основам информатики. 
		Координирует работу тренинг-центра компании. 
		Кандидат физико-математических наук, доцент. Увлекается фотографией.', '98224AF6-9EF0-41A9-AC7F-100195E4202D','2015-01-12'),
	('44E8585C-7010-4A9E-8072-08F6C4638318','E07C1DB6-EFCE-44EB-B1E9-7A1A31B32FB8',
		N'Разработчик программного обеспечения с десятилетним опытом работы. 
		Проводит практические тренинги по Java Core и Java Web Development для начинающих Java-разработчиков. 
		Увлекается картингом и настольными играми.', '2618B559-73F8-4BE6-B48A-664A426EAA62', '2014-08-25')