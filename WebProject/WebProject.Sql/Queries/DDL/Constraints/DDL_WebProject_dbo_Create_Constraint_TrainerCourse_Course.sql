﻿ALTER TABLE dbo.TrainerCourses
	ADD CONSTRAINT FK_TrainerCourseRef_Course
	FOREIGN KEY (CourseId)
	REFERENCES dbo.Courses (CourseId)
