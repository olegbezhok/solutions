﻿ALTER TABLE dbo.Trainers
	ADD CONSTRAINT FK_Trainer_User
	FOREIGN KEY (UserId)
	REFERENCES dbo.[Users] (UserId)
