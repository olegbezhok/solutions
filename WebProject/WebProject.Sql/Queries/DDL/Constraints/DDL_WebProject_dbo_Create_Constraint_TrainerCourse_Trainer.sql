﻿ALTER TABLE dbo.TrainerCourses
	ADD CONSTRAINT FK_TrainerCourse_Trainer
	FOREIGN KEY (TrainerId)
	REFERENCES dbo.Trainers (TrainerId)
