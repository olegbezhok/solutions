﻿ALTER TABLE dbo.Trainers
	ADD CONSTRAINT FK_Trainer_TrainerGroup
	FOREIGN KEY (TrainerGroupId)
	REFERENCES dbo.TrainerGroups (TrainerGroupId)
