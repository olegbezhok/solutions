﻿ALTER TABLE dbo.[Users]
	ADD CONSTRAINT FK_User_DepartamentType
	FOREIGN KEY (DepartamentTypeId)
	REFERENCES dbo.DepartamentTypes (DepartamentTypeId)
