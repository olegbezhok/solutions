﻿ALTER TABLE [dbo].Courses
	ADD CONSTRAINT FK_Course_CourseGroup
	FOREIGN KEY (CourseGroupId)
	REFERENCES CourseGroups (CourseGroupId)
