﻿CREATE TABLE dbo.CourseGroups(
	CourseGroupId UNIQUEIDENTIFIER,
	[Description] nvarchar(20) NOT NULL,

	CONSTRAINT PK_CourseType PRIMARY KEY (CourseGroupId)
)