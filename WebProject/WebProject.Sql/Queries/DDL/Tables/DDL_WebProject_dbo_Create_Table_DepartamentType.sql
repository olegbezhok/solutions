﻿CREATE TABLE dbo.DepartamentTypes(
	DepartamentTypeId UNIQUEIDENTIFIER,
	[Description] nvarchar(20) NOT NULL

	CONSTRAINT PK_DepartamentType PRIMARY KEY (DepartamentTypeId)
)