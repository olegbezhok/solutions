﻿CREATE TABLE dbo.TrainerGroups(
	TrainerGroupId UNIQUEIDENTIFIER,
	[Description] nvarchar(40) NOT NULL,

	CONSTRAINT PK_TrainerGroup PRIMARY KEY (TrainerGroupId)
)