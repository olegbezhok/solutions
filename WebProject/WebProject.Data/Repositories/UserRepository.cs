﻿using System.Data.Entity.Migrations;
using WebProject.Data.Context;
using WebProject.Data.Contracts.Entities;
using WebProject.Data.Contracts.Repositories;

namespace WebProject.Data.Repositories
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        public UserRepository(WebProjectContext context) : base(context)
        {
        }

        public void Delete(User user)
        {
            user.IsDelete = true;
            _dbSet.AddOrUpdate(user);
        }
    }
}
