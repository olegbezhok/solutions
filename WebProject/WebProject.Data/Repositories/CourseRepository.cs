﻿using System.Collections.Generic;
using System.Linq;
using WebProject.Data.Context;
using WebProject.Data.Contracts.Entities;
using WebProject.Data.Contracts.Repositories;

namespace WebProject.Data.Repositories
{
    public class CourseRepository : GenericDateDependRepository<Course>, ICourseRepository
    {
        public CourseRepository(WebProjectContext context) : base(context)
        {
        }

        public IEnumerable<CourseGroup> GetCourseGroup()
        {
            return _context.CourseGroups.ToList();
        }
    }
}
