﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using WebProject.Data.Context;
using WebProject.Data.Contracts.Repositories;

namespace WebProject.Data.Repositories
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        protected readonly WebProjectContext _context;
        protected readonly DbSet<TEntity> _dbSet;

        public GenericRepository(WebProjectContext context)
        {
            _context = context;
            _dbSet = context.Set<TEntity>();
        }

        public TEntity Add(TEntity entity)
        {
            return _dbSet.Add(entity);
        }
        
        public IEnumerable<TEntity> GetAll()
        {
            return _dbSet.ToList();
        }

        public TEntity GetById(Guid id)
        {
            return _dbSet.Find(id);
        }

        public void Update(TEntity entityToUpdate)
        {
            _dbSet.AddOrUpdate(entityToUpdate);
        }
    }
}
