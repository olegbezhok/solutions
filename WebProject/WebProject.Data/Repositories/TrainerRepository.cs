﻿using System.Collections.Generic;
using System.Linq;
using WebProject.Data.Context;
using WebProject.Data.Contracts.Entities;
using WebProject.Data.Contracts.Repositories;

namespace WebProject.Data.Repositories
{
    public class TrainerRepository : GenericDateDependRepository<Trainer>, ITrainerRepository
    {
        public TrainerRepository(WebProjectContext context) : base(context)
        {
        }

        public IEnumerable<TrainerGroup> GetTrainerGroups()
        {
            return _context.TrainerGroups.ToList();
        }

        public IEnumerable<Trainer> GetShotInfoTrainer()
        {
            return _dbSet.ToList().Where(x=> x.EndDate == null && x.User.IsDelete == false);
        }
    }
}
