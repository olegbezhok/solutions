﻿using System;
using System.Data.Entity.Migrations;
using WebProject.Data.Context;
using WebProject.Data.Contracts.Entities;
using WebProject.Data.Contracts.Repositories;

namespace WebProject.Data.Repositories
{
    public class GenericDateDependRepository<TEntity> : GenericRepository<TEntity>, IGenericDateDependRepository<TEntity> where TEntity : DateDependentEntities
    {
        public GenericDateDependRepository(WebProjectContext context) : base(context)
        {
        }

        public void Delete(TEntity entity)
        {
            entity.EndDate = DateTime.Now;
            _dbSet.AddOrUpdate(entity);
        }
    }
}
