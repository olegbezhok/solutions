﻿using WebProject.Data.Context;
using WebProject.Data.Contracts.Entities;
using WebProject.Data.Contracts.Repositories;

namespace WebProject.Data.Repositories
{
    public class TrainerCourseRepository: GenericDateDependRepository<TrainerCourse>, ITrainerCourseRepository
    {
        public TrainerCourseRepository(WebProjectContext context) : base(context)
        {
        }
    }
}
