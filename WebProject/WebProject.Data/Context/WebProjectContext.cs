using System.Data.Entity;
using WebProject.Data.Contracts.Entities;
using WebProject.Data.Contracts.EntitiesManager;
using WebProject.Data.EntitiesManager;

namespace WebProject.Data.Context
{
    public class WebProjectContext : DbContext
    {
        public WebProjectContext()
            : base("name=WebProjectContext")
        {
        }

        public virtual DbSet<Course> Courses { get; set; }
        public virtual DbSet<CourseGroup> CourseGroups { get; set; }
        public virtual DbSet<DepartamentType> DepartamentTypes { get; set; }
        public virtual DbSet<Trainer> Trainers { get; set; }
        public virtual DbSet<TrainerCourse> TrainerCourses { get; set; }
        public virtual DbSet<TrainerGroup> TrainerGroups { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
           ICourseManager courseManager = new CourseManager(modelBuilder);
            courseManager.Initialize();
            courseManager.SetValidation();

            ITrainerManager trainerManager = new TrainerManager(modelBuilder);
            trainerManager.Initialize();
            trainerManager.SetValidation();

            ICourseGroupManager courseGroupManager = new CourseGroupManager(modelBuilder);
            courseGroupManager.Initialize();
            courseGroupManager.SetValidation();

            IDepartamentTypeManager departamentTypeManager = new DepartamentTypeManager(modelBuilder);
            departamentTypeManager.Initialize();
            departamentTypeManager.SetValidation();

            ITrainerGroupManager trainerGroupManager = new TrainerGroupManager(modelBuilder);
            trainerGroupManager.Initialize();
            trainerGroupManager.SetValidation();

            IUserManager userManager = new UserManager(modelBuilder);
            userManager.Initialize();
            userManager.SetValidation();
        }
    }
}
