﻿using FluentValidation;
using System.Data.Entity;
using WebProject.Data.Contracts.Entities;
using WebProject.Data.Contracts.EntitiesManager;

namespace WebProject.Data.EntitiesManager
{
    public class CourseGroupManager : AbstractValidator<CourseGroup>, ICourseGroupManager
    {
        private DbModelBuilder _modelBuilder;

        public CourseGroupManager(DbModelBuilder modelBuilder)
        {
            _modelBuilder = modelBuilder;
        }

        public void Initialize()
        {
            _modelBuilder.Entity<CourseGroup>()
                .HasMany(e => e.Courses)
                .WithRequired(e => e.CourseGroup)
                .WillCascadeOnDelete(false);
        }

        public void SetValidation()
        {
            RuleFor(c => c.Description).Length(2, 20).NotNull();
        }
    }
}
