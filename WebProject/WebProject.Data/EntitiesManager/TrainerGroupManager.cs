﻿using System.Data.Entity;
using FluentValidation;
using WebProject.Data.Contracts.Entities;
using WebProject.Data.Contracts.EntitiesManager;

namespace WebProject.Data.EntitiesManager
{
    public class TrainerGroupManager : AbstractValidator<TrainerGroup>,ITrainerGroupManager
    {
        private DbModelBuilder _modelBuilder;

        public TrainerGroupManager(DbModelBuilder modelBuilder)
        {
            _modelBuilder = modelBuilder;
        }

        public void Initialize()
        {
            _modelBuilder.Entity<TrainerGroup>()
                .HasMany(e => e.Trainers)
                .WithRequired(e => e.TrainerGroup)
                .WillCascadeOnDelete(false);
        }

        public void SetValidation()
        {
            RuleFor(c => c.Description).Length(2, 40).NotNull();
        }
    }
}
