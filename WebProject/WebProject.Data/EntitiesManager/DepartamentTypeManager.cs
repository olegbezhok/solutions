﻿using System.Data.Entity;
using FluentValidation;
using WebProject.Data.Contracts.Entities;
using WebProject.Data.Contracts.EntitiesManager;

namespace WebProject.Data.EntitiesManager
{
    public class DepartamentTypeManager : AbstractValidator<DepartamentType>, IDepartamentTypeManager
    {
        private DbModelBuilder _modelBuilder;

        public DepartamentTypeManager(DbModelBuilder modelBuilder)
        {
            _modelBuilder = modelBuilder;
        }

        public void Initialize()
        {
            _modelBuilder.Entity<DepartamentType>()
                .HasMany(e => e.Users)
                .WithRequired(e => e.DepartamentType)
                .WillCascadeOnDelete(false);
        }

        public void SetValidation()
        {
            RuleFor(c => c.Description).Length(2, 20).NotNull();
        }
    }
}
