﻿using System.Data.Entity;
using WebProject.Data.Contracts.Entities;
using WebProject.Data.Contracts.EntitiesManager;
using FluentValidation;

namespace WebProject.Data.EntitiesManager
{
    public class UserManager : AbstractValidator<User>, IUserManager
    {
        private DbModelBuilder _modelBuilder;

        public UserManager(DbModelBuilder modelBuilder)
        {
            _modelBuilder = modelBuilder;
        }

        public void Initialize()
        {
            _modelBuilder.Entity<User>()
                .HasMany(e => e.Trainers)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);
        }

        public void SetValidation()
        {
            RuleFor(c => c.FullName).NotEmpty().Length(1,80).NotNull();
            RuleFor(c => c.Email).Length(5, 60).EmailAddress().NotNull();
            RuleFor(c => c.Role).NotNull();
            RuleFor(c => c.DepartamentTypeId).NotNull();
            RuleFor(c => c.Location).Length(5, 50).NotNull();
            RuleFor(c => c.Login).Length(5, 60).NotNull();
            RuleFor(c => c.Pass).Length(10, 30).NotNull();
            RuleFor(c => c.StartDate).NotNull();
        }
    }
}
