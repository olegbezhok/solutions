﻿using System.Data.Entity;
using FluentValidation;
using WebProject.Data.Contracts.Entities;
using WebProject.Data.Contracts.EntitiesManager;

namespace WebProject.Data.EntitiesManager
{
    public class TrainerManager : AbstractValidator<Trainer>,ITrainerManager
    {
        private DbModelBuilder _modelBuilder;

        public TrainerManager(DbModelBuilder modelBuilder)
        {
            _modelBuilder = modelBuilder;
        }

        public void Initialize()
        {
            _modelBuilder.Entity<Trainer>()
                .HasMany(e => e.TrainerCourses)
                .WithRequired(e => e.Trainer)
                .WillCascadeOnDelete(false);
        }

        public void SetValidation()
        {
            RuleFor(c => c.TrainerId).NotNull();
            RuleFor(c => c.UserId).NotNull();
            RuleFor(c => c.TrainerInfo).Length(0, 500);
            RuleFor(c => c.TrainerGroupId).NotNull();
            RuleFor(c => c.StartDate).NotNull();
        }
    }
}
