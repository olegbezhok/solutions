﻿using System.Data.Entity;
using FluentValidation;
using WebProject.Data.Contracts.Entities;
using WebProject.Data.Contracts.EntitiesManager;

namespace WebProject.Data.EntitiesManager
{
    public class CourseManager : AbstractValidator<Course>,ICourseManager
    {
        private DbModelBuilder _modelBuilder;

        public CourseManager(DbModelBuilder modelBuilder)
        {
            _modelBuilder = modelBuilder;
        }

        public void Initialize()
        {
            _modelBuilder.Entity<Course>()
                .HasMany(e => e.TrainerCourses)
                .WithRequired(e => e.Course)
                .WillCascadeOnDelete(false);
        }

        public void SetValidation()
        {
            RuleFor(c => c.CourseName).Length(2, 40).NotNull();
            RuleFor(c => c.CourseTypeId).NotNull();
            RuleFor(c => c.CourseGroupId).NotNull();
            RuleFor(c => c.Description).Length(10, 500).NotNull();
            RuleFor(c => c.StartDate).NotNull();
        }
    }
}
