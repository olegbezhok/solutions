﻿using System;
using WebProject.Data.Context;
using WebProject.Data.Contracts.Repositories;
using WebProject.Data.Contracts.UnitOfWork;
using WebProject.Data.Repositories;

namespace WebProject.Data.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly WebProjectContext _context;
        private bool _disposed;

        private ITrainerRepository TrainerRepo { get; set; }
        private ICourseRepository CourseRepo { get; set; }
        private IUserRepository UserRepo { get; set; }
        private ITrainerCourseRepository TrainerCourseRepo { get; set; }

        public UnitOfWork(WebProjectContext context)
        {
            _context = context;
        }

        public ITrainerRepository TrainerRepository =>
            TrainerRepo ?? (TrainerRepo = new TrainerRepository(_context));

        public ICourseRepository CourseRepository =>
            CourseRepo ?? (CourseRepo = new CourseRepository(_context));

        public IUserRepository UserRepository =>
            UserRepo ?? (UserRepo = new UserRepository(_context));

        public ITrainerCourseRepository TrainerCourseRepository =>
            TrainerCourseRepo ?? (TrainerCourseRepo = new TrainerCourseRepository(_context));

        public void Save()
        {
            _context.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }
    }
}
