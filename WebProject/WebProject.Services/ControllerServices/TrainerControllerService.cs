﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebProject.Data.Contracts.Entities;
using WebProject.Data.Contracts.Entities.Enums;
using WebProject.Data.Contracts.UnitOfWork;
using WebProject.Services.Contracts.ControllerServices;
using WebProject.Services.Contracts.Model.DTOs;
using WebProject.Services.ServiceHelper;

namespace WebProject.Services.ControllerServices
{
    public class TrainerControllerService : ITrainerControllerService
    {
        private IUnitOfWork _unitOfWork;

        public TrainerControllerService(IUnitOfWork uow)
        {
            _unitOfWork = uow;
        }

        public TrainerDTO Add(TrainerDTO trainer)
        {
            _unitOfWork.TrainerRepository.Add(AutoMapper.Mapper.Map<TrainerDTO, Trainer>(trainer));
            _unitOfWork.UserRepository.GetById(trainer.UserId).Role = (int)RoleType.Trainer;
            _unitOfWork.Save();
            return trainer;
        }

        public void Delete(TrainerDTO trainer)
        {
            var deleteDate = DateTime.Now;
            foreach (var trainerCourse in trainer.TrainerCourses)
            {
                trainerCourse.EndDate = deleteDate;
                _unitOfWork.TrainerCourseRepository.Delete(AutoMapper.Mapper.Map < TrainerCourseDTO, TrainerCourse> (trainerCourse));
            }
            _unitOfWork.TrainerRepository.Delete(AutoMapper.Mapper.Map<TrainerDTO, Trainer>(trainer));
            _unitOfWork.Save();
        }

        public IEnumerable<TrainerDTO> GetAll()
        {
            var trainerList =
                AutoMapper.Mapper.Map<IEnumerable<Trainer>, IEnumerable<TrainerDTO>>(_unitOfWork.TrainerRepository
                    .GetAll());
            foreach (var trainer in trainerList)
            {
                DateFormatter.ChangeDateFormat(trainer);
            }

            return trainerList;

        }

        public TrainerDTO GetById(Guid id)
        {
            var trainer = AutoMapper.Mapper.Map<Trainer, TrainerDTO>(_unitOfWork.TrainerRepository.GetById(id));
            DateFormatter.ChangeDateFormat(trainer);

            return trainer;
        }

        public TrainerDTO GetTrainerByUserId(Guid id)
        {
            return AutoMapper.Mapper.Map<Trainer, TrainerDTO>(_unitOfWork.UserRepository.GetById(id).Trainers
                .FirstOrDefault(x => x.EndDate == null));
        }

        public IEnumerable<TrainerGroupDTO> GetTrainerGroups()
        {
            return AutoMapper.Mapper.Map<IEnumerable<TrainerGroup>, IEnumerable<TrainerGroupDTO>>(_unitOfWork.TrainerRepository.GetTrainerGroups());
        }

        public void Update(TrainerDTO trainer)
        {
            _unitOfWork.TrainerRepository.Update(AutoMapper.Mapper.Map<TrainerDTO, Trainer>(trainer));
            _unitOfWork.Save();
        }
    }
}
