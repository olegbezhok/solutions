﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebProject.Data.Contracts.Entities;
using WebProject.Data.Contracts.UnitOfWork;
using WebProject.Services.Contracts.ControllerServices;
using WebProject.Services.Contracts.Model.DTOs;
using WebProject.Services.Contracts.Model.InputModels;
using WebProject.Services.ServiceHelper;
using WebProject.Services.ServiceHelper.PasswordHasher;

namespace WebProject.Services.ControllerServices
{
    public class UserControllerService : IUserControllerService
    {
        private PasswordManager _pwdManager;
        private IUnitOfWork _unitOfWork;

        public UserControllerService(IUnitOfWork uow)
        {
            _unitOfWork = uow;
            _pwdManager = new PasswordManager();
        }

        public UserDTO Add(UserDTO user)
        {
            user.Pass = _pwdManager.GeneratePasswordHash(user.Pass, out string salt);
            user.Salt = salt;
            _unitOfWork.UserRepository.Add(AutoMapper.Mapper.Map<UserDTO, User>(user));
            _unitOfWork.Save();
            return user;
        }

        public void Delete(UserDTO user)
        {
            var date = DateTime.Now;
            foreach (var trainer in user.Trainers.Where(x => x.EndDate == null))
            {
                trainer.EndDate = date;

                foreach (var course in trainer.TrainerCourses.Where(x=> x.EndDate == null))
                {
                    course.EndDate = date;
                    _unitOfWork.TrainerCourseRepository.Delete(AutoMapper.Mapper.Map<TrainerCourseDTO, TrainerCourse>(course));
                }
                _unitOfWork.TrainerRepository.Delete(AutoMapper.Mapper.Map<TrainerDTO, Trainer>(trainer));
                _unitOfWork.Save();
            }

            
            _unitOfWork.UserRepository.Delete(AutoMapper.Mapper.Map<UserDTO, User>(user));
            _unitOfWork.Save();
        }

        public IEnumerable<UserDTO> GetAll()
        {
            return AutoMapper.Mapper.Map<List<User>, List<UserDTO>>(_unitOfWork.UserRepository.GetAll().ToList());
        }

        public UserDTO GetById(Guid id)
        {
            var user = AutoMapper.Mapper.Map<User, UserDTO>(_unitOfWork.UserRepository.GetById(id));
            user.FormatedDate = DateFormatter.ChangeFormat(user.StartDate);
            return user;
        }

        public void Update(UserDTO user)
        {
            _unitOfWork.UserRepository.Update(AutoMapper.Mapper.Map<UserDTO, User>(user));
            _unitOfWork.Save();
        }

        public UserDTO Login(LoginUserModel loginModel)
        {
            var user = AutoMapper.Mapper.Map<User, UserDTO>(_unitOfWork.UserRepository.GetAll()
                .Where(x => x.Login == loginModel.Login && x.IsDelete == false).FirstOrDefault());
            if (user == null)
            {
                throw new ArgumentNullException("Login", "wrong login");
            }

            bool result = _pwdManager.IsPasswordMatch(loginModel.Pass, user.Salt, user.Pass);
            if (!result)
            {
                throw new ArgumentNullException("Pass", "wrong password");
            }
            else
            {
                return user;
            }
        }
    }
}
