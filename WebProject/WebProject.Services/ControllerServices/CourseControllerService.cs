﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebProject.Data.Contracts.Entities;
using WebProject.Data.Contracts.UnitOfWork;
using WebProject.Services.Contracts.ControllerServices;
using WebProject.Services.Contracts.Model.DTOs;
using WebProject.Services.ServiceHelper;

namespace WebProject.Services.ControllerServices
{
    public class CourseControllerService : ICourseControllerService
    {
        private IUnitOfWork _unitOfWork;

        public CourseControllerService(IUnitOfWork uof)
        {
            _unitOfWork = uof;
        }

        public CourseDTO Add(CourseDTO course)
        {
            foreach (var guid in course.TrainerCourseGuids)
            {
                course.TrainerCourses.Add(new TrainerCourseDTO()
                {
                    CourseId = course.CourseId,
                    TrainerId = guid,
                    StartDate = course.StartDate,
                });
            }
            _unitOfWork.CourseRepository.Add(AutoMapper.Mapper.Map<CourseDTO, Course>(course));
            _unitOfWork.Save();
            return course;
        }

        public void Delete(CourseDTO course)
        {
            foreach (var trainerCourse in course.TrainerCourses.Where(x=> x.EndDate == null))
            {
                _unitOfWork.TrainerCourseRepository.Delete(AutoMapper.Mapper.Map<TrainerCourseDTO, TrainerCourse>(trainerCourse));
            }
            _unitOfWork.CourseRepository.Delete(AutoMapper.Mapper.Map<CourseDTO, Course>(course));
            _unitOfWork.Save();
        }

        public IEnumerable<CourseDTO> GetAll()
        {
            var courseList = AutoMapper.Mapper.Map<IEnumerable<Course>, IEnumerable<CourseDTO>>(_unitOfWork.CourseRepository.GetAll());
            foreach (var course in courseList)
            {
                DateFormatter.ChangeDateFormat(course);
            }

            return courseList;
        }

        public CourseDTO GetById(Guid id)
        {
            var course = AutoMapper.Mapper.Map<Course, CourseDTO>(_unitOfWork.CourseRepository.GetById(id));
            foreach (var trainerCourse in course.TrainerCourses.Where(x => x.EndDate != null).ToList())
            {
                course.TrainerCourses.Remove(trainerCourse);
            }
            DateFormatter.ChangeDateFormat(course);
            return course;
        }

        public IEnumerable<TrainerDTO> GetTrainerShortInfos()
        {
            return AutoMapper.Mapper.Map<IEnumerable<Trainer>, IEnumerable<TrainerDTO>>(_unitOfWork.TrainerRepository.GetShotInfoTrainer());
        }

        public IEnumerable<CourseGroupDTO> GetCourseGroup()
        {
            return AutoMapper.Mapper.Map<IEnumerable<CourseGroup>, IEnumerable<CourseGroupDTO>>(_unitOfWork.CourseRepository.GetCourseGroup());
        }

        public void Update(CourseDTO course)
        {
            var deleteTime = DateTime.Now;
            var oldCourse = GetById(course.CourseId);
            course.StartDate = Convert.ToDateTime(course.FormatedStartDate);
            if (course.FormatedEndDate != null)
            {
                course.EndDate = Convert.ToDateTime(course.FormatedEndDate);
            }
            foreach (var guid in course.TrainerCourseGuids)
            {
                var oldTrainer =
                    oldCourse.TrainerCourses.FirstOrDefault(x => x.TrainerId == guid && x.EndDate == null);
                if (oldTrainer == null)
                {
                    course.TrainerCourses.Add(new TrainerCourseDTO()
                    {
                        CourseId = course.CourseId,
                        TrainerId = guid,
                        StartDate = deleteTime,
                    });
                }
                oldCourse.TrainerCourses.Remove(oldTrainer);
            }

            if (oldCourse.TrainerCourses.Count != 0)
            {
                foreach (var trainerCourse in oldCourse.TrainerCourses)
                {
                    if (trainerCourse.EndDate == null)
                    {
                        trainerCourse.EndDate = deleteTime;
                        course.TrainerCourses.Add(trainerCourse);
                    }
                }
            }

            _unitOfWork.CourseRepository.Update(AutoMapper.Mapper.Map<CourseDTO, Course>(course));
            _unitOfWork.Save();

            var trainerCoursesOnUpdate =
                AutoMapper.Mapper.Map<IEnumerable<TrainerCourseDTO>, IEnumerable<TrainerCourse>>(course.TrainerCourses);
            foreach (var trainerCourse in trainerCoursesOnUpdate)
            {
                _unitOfWork.TrainerCourseRepository.Update(trainerCourse);
            }
            _unitOfWork.Save();
        }

        public IEnumerable<CourseDTO> GetMyCourses(Guid id)
        {
            IList<CourseDTO> courses = new List<CourseDTO>();
            var allCourses = AutoMapper.Mapper.Map<IEnumerable<Course>, IEnumerable<CourseDTO>>(_unitOfWork.CourseRepository.GetAll());
            foreach (var course in allCourses)
            {
                if (course.TrainerCourses.Where(x => x.Trainer.UserId == id && x.EndDate == null).Count() != 0)
                {
                    courses.Add(course);
                }
            }

            return courses;
        }
    }
}
