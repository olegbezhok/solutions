﻿using WebProject.Data.Context;
using WebProject.Data.Contracts.UnitOfWork;
using WebProject.Data.UnitOfWork;

namespace WebProject.Services.ServiceHelper
{
    public class ServiceSaveChangesHelper
    {
        public ServiceSaveChangesHelper()
        {
        }

        public void SaveChanges()
        {
            IUnitOfWork uof = new UnitOfWork(new WebProjectContext());
            uof.Save();
        }
    }
}
