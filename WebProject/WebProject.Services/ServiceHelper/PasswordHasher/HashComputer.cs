﻿using System.Security.Cryptography;
using System.Text;

namespace WebProject.Services.ServiceHelper.PasswordHasher
{
    public class HashComputer
    {
        public string GetPasswordHashAndSalt(string message)
        {
            SHA512 sha = new SHA512Managed();
            byte[] dataBytes = Encoding.Default.GetBytes(message);
            byte[] resultBytes = sha.ComputeHash(dataBytes);
            
            return Encoding.Default.GetString(resultBytes);
        }
    }
}
