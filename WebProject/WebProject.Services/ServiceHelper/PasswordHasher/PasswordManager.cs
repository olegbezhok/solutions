﻿namespace WebProject.Services.ServiceHelper.PasswordHasher
{
    public class PasswordManager
    {
        private HashComputer _hashComputer = new HashComputer();

        public string GeneratePasswordHash(string plainTextPassword, out string salt)
        {
            salt = SaltGenerator.GetSaltString();

            string finalString = plainTextPassword + salt;

            return _hashComputer.GetPasswordHashAndSalt(finalString);
        }

        public bool IsPasswordMatch(string password, string salt, string hash)
        {
            string finalString = password + salt;
            return hash == _hashComputer.GetPasswordHashAndSalt(finalString);
        }
    }
}
