﻿
using System.Security.Cryptography;
using System.Text;

namespace WebProject.Services.ServiceHelper.PasswordHasher
{
    public class SaltGenerator
    {
        private static RNGCryptoServiceProvider _cryptoServiceProvider = null;
        private const int _saltSize = 24;

        static SaltGenerator()
        {
            _cryptoServiceProvider = new RNGCryptoServiceProvider();
        }

        public static string GetSaltString()
        {
            byte[] saltBytes = new byte[_saltSize];
            _cryptoServiceProvider.GetNonZeroBytes(saltBytes);
            string saltString = Encoding.Default.GetString(saltBytes);

            return saltString;
        }
    }
}
