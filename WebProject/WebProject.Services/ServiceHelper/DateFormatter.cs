﻿using System;
using WebProject.Services.Contracts.Model.Helpers;

namespace WebProject.Services.ServiceHelper
{
    public static class DateFormatter
    {
        public static void ChangeDateFormat(DateDependModel model)
        {
            model.FormatedStartDate = ChangeFormat(model.StartDate.Value);
            if (model.EndDate != null)
            {
                model.FormatedEndDate = ChangeFormat(model.EndDate.Value);
            }
            else
            {
                model.FormatedEndDate = "n/a";
            }
        }

        public static string ChangeFormat(DateTime date)
        {
            return date.ToString("yyyy-MM-dd");
        }
    }
}
