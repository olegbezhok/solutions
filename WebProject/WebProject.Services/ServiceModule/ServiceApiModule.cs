﻿using Ninject;
using WebProject.Data.Contracts.UnitOfWork;
using WebProject.Data.UnitOfWork;

namespace WebProject.Services.ServiceModule
{
    public static class ServiceApiModule
    {
        public static void Load(IKernel kernel)
        {
            kernel.Bind<IUnitOfWork>().To<UnitOfWork>();
        }
    }
}
