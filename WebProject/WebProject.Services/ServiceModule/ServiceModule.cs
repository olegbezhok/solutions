﻿using Ninject.Modules;
using Ninject.Web.Common;
using WebProject.Data.Contracts.UnitOfWork;
using WebProject.Data.UnitOfWork;

namespace WebProject.Services.ServiceModule
{
    public class ServiceModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUnitOfWork>().To<UnitOfWork>().InRequestScope();
        }
    }
}
