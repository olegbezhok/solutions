﻿using AutoMapper;
using System;
using WebProject.Services.Mappers.Profilers;

namespace WebProject.Services.Mappers
{
    public class ServiceMapperConfig
    {
        public static Action<IMapperConfigurationExpression> ConfigAction = cfg =>
        {
            cfg.Advanced.AllowAdditiveTypeMapCreation = true;
            cfg.AddProfile<UserProfile>();
            cfg.AddProfile<TrainerProfile>();
            cfg.AddProfile<UserProfile>();
            cfg.AddProfile<CourseProfile>();
            cfg.AddProfile<TrainerCourseProfile>();
            cfg.AddProfile<TrainerGroupProfile>();
        };
    }
}
