﻿using AutoMapper;
using WebProject.Data.Contracts.Entities;
using WebProject.Services.Contracts.Model.DTOs;

namespace WebProject.Services.Mappers.Profilers
{
    class TrainerProfile : Profile
    {
        public TrainerProfile()
        {
            this.CreateMap<Trainer, TrainerDTO>()
                .ReverseMap()
                .Ignore(src => src.TrainerGroup)
                .Ignore(src => src.TrainerCourses);
        }
    }
}
