﻿using AutoMapper;
using WebProject.Data.Contracts.Entities;
using WebProject.Services.Contracts.Model.DTOs;

namespace WebProject.Services.Mappers.Profilers
{
    public class CourseProfile : Profile
    {
        public CourseProfile()
        {
            this.CreateMap<Course, CourseDTO>().Ignore(src => src.TrainerCourseGuids).ReverseMap();
            this.CreateMap<CourseGroup, CourseGroupDTO>().ReverseMap();
        }
    }
}
