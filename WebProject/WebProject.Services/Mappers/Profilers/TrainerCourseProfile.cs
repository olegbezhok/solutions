﻿using AutoMapper;
using WebProject.Data.Contracts.Entities;
using WebProject.Services.Contracts.Model.DTOs;

namespace WebProject.Services.Mappers.Profilers
{
    public class TrainerCourseProfile : Profile
    {
        public TrainerCourseProfile()
        {
            this.CreateMap<TrainerCourse, TrainerCourseDTO>().Ignore(src => src.FormatedEndDate)
                .Ignore(src => src.FormatedStartDate)
                .ReverseMap();
        }
    }
}
