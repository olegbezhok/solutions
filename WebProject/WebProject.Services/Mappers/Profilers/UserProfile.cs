﻿using AutoMapper;
using WebProject.Data.Contracts.Entities;
using WebProject.Services.Contracts.Model.DTOs;

namespace WebProject.Services.Mappers.Profilers
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            this.CreateMap<User, UserDTO>().Ignore(src=> src.FormatedDate)
                .ReverseMap().Ignore(src => src.Trainers);
        }
    }
}
