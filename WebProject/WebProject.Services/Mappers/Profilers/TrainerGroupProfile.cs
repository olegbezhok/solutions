﻿using AutoMapper;
using WebProject.Data.Contracts.Entities;
using WebProject.Services.Contracts.Model.DTOs;

namespace WebProject.Services.Mappers.Profilers
{
    public class TrainerGroupProfile : Profile
    {
        public TrainerGroupProfile()
        {
            this.CreateMap<TrainerGroup, TrainerGroupDTO>().ReverseMap();
        }
    }
}
