﻿using System;
using System.Security.Principal;
using WebProject.Services.Contracts.CustomPrincipal;

namespace WebProject.Services.CustomPrincipal
{
    public class CustomPrincipal : ICustomPrincipal
    {
        public IIdentity Identity { get; private set; }

        public CustomPrincipal(string login)
        {
            this.Identity = new GenericIdentity(login);
        }

        public bool IsInRole(string role)
        {
            return Role == role;
        }

        public Guid Id { get; set; }
        public string FullName { get; set; }
        public string Role { get; set; }
    }
}
