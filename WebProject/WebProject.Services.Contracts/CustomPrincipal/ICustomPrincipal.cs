﻿using System;
using System.Security.Principal;

namespace WebProject.Services.Contracts.CustomPrincipal
{
    public interface ICustomPrincipal : IPrincipal
    {
        Guid Id { get; set; }

        string FullName { get; set; }

        string Role { get; set; }
    }
}
