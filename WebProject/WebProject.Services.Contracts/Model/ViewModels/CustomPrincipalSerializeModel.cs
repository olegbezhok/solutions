﻿using System;

namespace WebProject.Services.Contracts.Model.ViewModels
{
    public class CustomPrincipalSerializeModel
    {
        public Guid Id { get; set; }

        public string FullName { get; set; }

        public string Role { get; set; }
    }
}
