﻿using System;

namespace WebProject.Services.Contracts.Model.ViewModels
{
    public class TrainerViewModel
    {
        public Guid TrainerId { get; set; }

        public string TrainerInfo { get; set; }

        public UserViewModel User { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string FormatedStartDate { get; set; }

        public string FormatedEndDate { get; set; }

        public Guid TrainerGroupId { get; set; }
    }
}
