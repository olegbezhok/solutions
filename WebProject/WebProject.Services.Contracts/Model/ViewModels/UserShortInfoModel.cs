﻿using System;

namespace WebProject.Services.Contracts.Model.ViewModels
{
    public class UserShortInfoModel
    {
        public Guid UserId { get; set; }

        public string FullName { get; set; }
    }
}
