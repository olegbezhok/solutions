﻿using System;
using System.Collections.Generic;

namespace WebProject.Services.Contracts.Model.ViewModels
{
    public class UserViewModel
    {
        public Guid UserId { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public Guid DepartamentId { get; set; }

        public byte[] Photo { get; set; }

        public string Location { get; set; }

        public DateTime StartDate { get; set; }

        public ICollection<TrainerViewModel> Trainers { get; set; }
    }
}
