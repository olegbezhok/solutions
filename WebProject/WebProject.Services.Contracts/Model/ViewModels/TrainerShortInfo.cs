﻿using System;

namespace WebProject.Services.Contracts.Model.ViewModels
{
    public class TrainerShortInfo
    {
        public Guid TrainerId { get; set; }

        public string FullName { get; set; }
    }
}
