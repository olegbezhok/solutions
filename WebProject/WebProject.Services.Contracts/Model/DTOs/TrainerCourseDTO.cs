﻿using System;
using WebProject.Services.Contracts.Model.Helpers;

namespace WebProject.Services.Contracts.Model.DTOs
{
    public class TrainerCourseDTO : DateDependModel
    {
        public Guid TrainerId { get; set; }
        
        public Guid CourseId { get; set; }

        public virtual CourseDTO Course { get; set; }

        public virtual TrainerDTO Trainer { get; set; }
    }
}
