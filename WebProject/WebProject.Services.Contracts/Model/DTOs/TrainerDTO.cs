﻿using System;
using System.Collections.Generic;
using WebProject.Services.Contracts.Model.Helpers;

namespace WebProject.Services.Contracts.Model.DTOs
{
    public class TrainerDTO : DateDependModel
    {
        public Guid TrainerId { get; set; }

        public Guid UserId { get; set; }
        
        public string TrainerInfo { get; set; }

        public Guid TrainerGroupId { get; set; }

        public virtual UserDTO User { get; set; }

        public virtual ICollection<TrainerCourseDTO> TrainerCourses { get; set; }
    }
}
