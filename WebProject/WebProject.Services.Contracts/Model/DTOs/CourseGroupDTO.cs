﻿using System;
using System.Collections.Generic;

namespace WebProject.Services.Contracts.Model.DTOs
{
    public class CourseGroupDTO
    {
        public Guid CourseGroupId { get; set; }

        public string Description { get; set; }

        public virtual ICollection<CourseDTO> Courses { get; set; }
    }
}
