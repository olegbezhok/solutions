﻿using System;
using System.Collections.Generic;
using WebProject.Services.Contracts.Model.Helpers;

namespace WebProject.Services.Contracts.Model.DTOs
{
    public class CourseDTO: DateDependModel
    {
        public CourseDTO()
        {
            TrainerCourseGuids = new List<Guid>();
            TrainerCourses = new List<TrainerCourseDTO>();
        }

        public Guid CourseId { get; set; }

        public string CourseName { get; set; }

        public int CourseTypeId { get; set; }

        public Guid CourseGroupId { get; set; }

        public string Description { get; set; }

        public virtual CourseGroupDTO CourseGroup { get; set; }

        public bool IsCanEdit { get; set; }

        public virtual IList<TrainerCourseDTO> TrainerCourses { get; set; }

        public virtual IList<Guid> TrainerCourseGuids { get; set; }
    }
}