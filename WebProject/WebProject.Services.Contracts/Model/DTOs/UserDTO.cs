﻿using System;
using System.Collections.Generic;

namespace WebProject.Services.Contracts.Model.DTOs
{
    public class UserDTO
    {
        public Guid UserId { get; set; }
        
        public string FullName { get; set; }
        
        public string Email { get; set; }

        public int Role { get; set; }

        public byte[] Photo { get; set; }

        public Guid DepartamentTypeId { get; set; }
        
        public string Location { get; set; }
        
        public string Login { get; set; }
        
        public string Pass { get; set; }

        public string Salt { get; set; }

        public DateTime StartDate { get; set; }

        public string FormatedDate { get; set; }

        public bool IsDelete { get; set; }

        public virtual ICollection<TrainerDTO> Trainers { get; set; }
    }
}
