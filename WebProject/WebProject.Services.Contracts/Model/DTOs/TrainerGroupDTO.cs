﻿using System;

namespace WebProject.Services.Contracts.Model.DTOs
{
    public class TrainerGroupDTO
    {
        public Guid TrainerGroupId { get; set; }

        public string Description { get; set; }
    }
}
