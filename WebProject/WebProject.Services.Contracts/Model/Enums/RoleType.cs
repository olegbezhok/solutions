﻿namespace WebProject.Services.Contracts.Model.Enums
{
    public enum RoleType
    {
        Admin = 1,
        Trainer = 2,
        ProjectManager = 3,
        TeamManager = 4
    }
}
