﻿namespace WebProject.Services.Contracts.Model.Enums
{
    public enum CourseType
    {
        Practical = 1,
        Lectures = 2
    }
}
