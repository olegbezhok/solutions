﻿using System;

namespace WebProject.Services.Contracts.Model.InputModels
{
    public class TrainerModel
    {
        public Guid TrainerId { get; set; }

        public Guid UserId { get; set; }

        public string TrainerInfo { get; set; }

        public Guid TrainerGroupId { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string FormatedStartDate { get; set; }

        public string FormatedEndDate { get; set; }
    }
}
