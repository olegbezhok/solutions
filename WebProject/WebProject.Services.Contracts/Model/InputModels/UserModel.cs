﻿using System;

namespace WebProject.Services.Contracts.Model.InputModels
{
    public class UserModel
    {
        public Guid UserId { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public byte[] Photo { get; set; }

        public int Role { get; set; }

        public Guid DepartamentTypeId { get; set; }

        public string Location { get; set; }

        public string Login { get; set; }

        public string Pass { get; set; }

        public bool IsDelete { get; set; }

        public DateTime StartDate { get; set; }
    }
}
