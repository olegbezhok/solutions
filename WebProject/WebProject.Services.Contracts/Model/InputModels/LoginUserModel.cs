﻿namespace WebProject.Services.Contracts.Model.InputModels
{
    public class LoginUserModel
    {
        public string Pass { get; set; }

        public string Login { get; set; }
    }
}
