﻿using System;

namespace WebProject.Services.Contracts.Model.Helpers
{
    public class DateDependModel
    {
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string FormatedStartDate { get; set; }

        public string FormatedEndDate { get; set; }
    }
}
