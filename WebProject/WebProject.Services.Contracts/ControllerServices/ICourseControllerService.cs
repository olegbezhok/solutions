﻿using System;
using System.Collections.Generic;
using WebProject.Services.Contracts.Model.DTOs;

namespace WebProject.Services.Contracts.ControllerServices
{
    public interface ICourseControllerService : IGenericControllerService<CourseDTO>
    {
        IEnumerable<TrainerDTO> GetTrainerShortInfos();
        IEnumerable<CourseGroupDTO> GetCourseGroup();
        IEnumerable<CourseDTO> GetMyCourses(Guid id);
    }
}
