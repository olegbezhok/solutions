﻿using System;
using System.Collections.Generic;

namespace WebProject.Services.Contracts.ControllerServices
{
    public interface IGenericControllerService<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> GetAll();

        TEntity GetById(Guid id);

        TEntity Add(TEntity entity);

        void Delete(TEntity entity);

        void Update(TEntity entity);
    }
}
