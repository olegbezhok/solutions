﻿using WebProject.Services.Contracts.Model.DTOs;
using WebProject.Services.Contracts.Model.InputModels;

namespace WebProject.Services.Contracts.ControllerServices
{
    public interface IUserControllerService : IGenericControllerService<UserDTO>
    {
        UserDTO Login(LoginUserModel loginModel);
    }
}
