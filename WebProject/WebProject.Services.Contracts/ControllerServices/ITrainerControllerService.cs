﻿using System;
using System.Collections.Generic;
using WebProject.Services.Contracts.Model.DTOs;

namespace WebProject.Services.Contracts.ControllerServices
{
    public interface ITrainerControllerService : IGenericControllerService<TrainerDTO>
    {
        IEnumerable<TrainerGroupDTO> GetTrainerGroups();
        TrainerDTO GetTrainerByUserId(Guid id);
    }
}
