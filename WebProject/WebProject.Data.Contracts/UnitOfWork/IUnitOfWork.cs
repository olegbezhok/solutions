﻿using System;
using WebProject.Data.Contracts.Repositories;

namespace WebProject.Data.Contracts.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        ICourseRepository CourseRepository { get; }

        ITrainerRepository TrainerRepository { get; }

        IUserRepository UserRepository { get; }

        ITrainerCourseRepository TrainerCourseRepository { get; }

        void Save();
    }
}
