﻿
namespace WebProject.Data.Contracts.EntitiesManager
{
    public interface IGenericManager<TEntity> where TEntity : class
    {
        void Initialize();
        void SetValidation();
    }
}
