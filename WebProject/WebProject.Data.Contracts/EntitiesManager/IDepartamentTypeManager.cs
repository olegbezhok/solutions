﻿using WebProject.Data.Contracts.Entities;

namespace WebProject.Data.Contracts.EntitiesManager
{
    public interface IDepartamentTypeManager : IGenericManager<DepartamentType>
    {
    }
}
