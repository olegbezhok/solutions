﻿using System.Collections.Generic;
using WebProject.Data.Contracts.Entities;

namespace WebProject.Data.Contracts.Repositories
{
    public interface ITrainerRepository : IGenericDateDependRepository<Trainer>
    {
        IEnumerable<TrainerGroup> GetTrainerGroups();
        IEnumerable<Trainer> GetShotInfoTrainer();
    }
}
