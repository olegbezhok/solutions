﻿using System.Collections.Generic;
using WebProject.Data.Contracts.Entities;

namespace WebProject.Data.Contracts.Repositories
{
    public interface ICourseRepository : IGenericDateDependRepository<Course>
    {
        IEnumerable<CourseGroup> GetCourseGroup();
    }
}
