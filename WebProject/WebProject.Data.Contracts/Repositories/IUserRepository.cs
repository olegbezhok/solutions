﻿using WebProject.Data.Contracts.Entities;

namespace WebProject.Data.Contracts.Repositories
{
    public interface IUserRepository : IGenericRepository<User>
    {
        void Delete(User User);
    }
}
