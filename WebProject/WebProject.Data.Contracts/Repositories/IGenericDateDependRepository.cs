﻿using WebProject.Data.Contracts.Entities;

namespace WebProject.Data.Contracts.Repositories
{
    public interface IGenericDateDependRepository<TEntity> : IGenericRepository<TEntity> where TEntity : DateDependentEntities
    {
        void Delete(TEntity entity);
    }
}
