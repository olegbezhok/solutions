﻿using System;
using System.Collections.Generic;

namespace WebProject.Data.Contracts.Repositories
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> GetAll();

        TEntity GetById(Guid id);

        TEntity Add(TEntity entity);

        void Update(TEntity entityToUpdate);
    }
}
