﻿using WebProject.Data.Contracts.Entities;

namespace WebProject.Data.Contracts.Repositories
{
    public interface ITrainerCourseRepository : IGenericDateDependRepository<TrainerCourse>
    {
    }
}
