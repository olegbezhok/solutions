using System;
using System.Collections.Generic;

namespace WebProject.Data.Contracts.Entities
{
    public class TrainerGroup
    {
        public TrainerGroup()
        {
            Trainers = new List<Trainer>();
        }

        public Guid TrainerGroupId { get; set; }
        
        public string Description { get; set; }
 
        public virtual ICollection<Trainer> Trainers { get; set; }
    }
}
