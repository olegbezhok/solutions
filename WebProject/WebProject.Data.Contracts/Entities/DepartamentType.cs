using System;
using System.Collections.Generic;

namespace WebProject.Data.Contracts.Entities
{
    public class DepartamentType
    {
        public DepartamentType()
        {
            Users = new List<User>();
        }

        public Guid DepartamentTypeId { get; set; }
        
        public string Description { get; set; }
       
        public virtual ICollection<User> Users { get; set; }
    }
}
