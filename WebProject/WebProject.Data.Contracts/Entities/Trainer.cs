using System;
using System.Collections.Generic;

namespace WebProject.Data.Contracts.Entities
{
    public class Trainer : DateDependentEntities
    {
        public Trainer()
        {
            TrainerCourses = new List<TrainerCourse>();
        }

        public Guid TrainerId { get; set; }

        public Guid UserId { get; set; }
        
        public string TrainerInfo { get; set; }

        public Guid TrainerGroupId { get; set; }

        public DateTime StartDate { get; set; }

        public virtual TrainerGroup TrainerGroup { get; set; }

        public virtual User User { get; set; }
    
        public virtual ICollection<TrainerCourse> TrainerCourses { get; set; }
    }
}
