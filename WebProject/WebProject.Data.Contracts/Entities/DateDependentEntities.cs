﻿using System;

namespace WebProject.Data.Contracts.Entities
{
    public class DateDependentEntities
    {
        public DateTime? EndDate { get; set; }
    }
}
