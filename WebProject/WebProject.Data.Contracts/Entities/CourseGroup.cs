using System;
using System.Collections.Generic;

namespace WebProject.Data.Contracts.Entities
{
    public class CourseGroup
    {
        public CourseGroup()
        {
            Courses = new List<Course>();
        }

        public Guid CourseGroupId { get; set; }
        
        public string Description { get; set; }
      
        public virtual ICollection<Course> Courses { get; set; }
    }
}
