using System;
using System.Collections.Generic;

namespace WebProject.Data.Contracts.Entities
{
    public class Course : DateDependentEntities
    {
        public Course()
        {
            TrainerCourses = new List<TrainerCourse>();
        }

        public Guid CourseId { get; set; }
        
        public string CourseName { get; set; }

        public int CourseTypeId { get; set; }

        public Guid CourseGroupId { get; set; }
        
        public string Description { get; set; }

        public DateTime StartDate { get; set; }

        public virtual CourseGroup CourseGroup { get; set; }
        
        public virtual ICollection<TrainerCourse> TrainerCourses { get; set; }
    }
}
