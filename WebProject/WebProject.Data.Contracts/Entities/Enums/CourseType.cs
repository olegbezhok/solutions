﻿namespace WebProject.Data.Contracts.Entities.Enums
{
    public enum  CourseType
    {
        Practical = 1,
        Lectures = 2
    }
}
