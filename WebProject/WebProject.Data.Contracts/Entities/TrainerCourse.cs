using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebProject.Data.Contracts.Entities
{
    public class TrainerCourse : DateDependentEntities
    {
        [Key]
        [Column(Order = 0)]
        public Guid TrainerId { get; set; }

        [Key]
        [Column(Order = 1)]
        public Guid CourseId { get; set; }

        [Key]
        [Column(Order = 2)]
        public DateTime StartDate { get; set; }

        public virtual Course Course { get; set; }

        public virtual Trainer Trainer { get; set; }
    }
}
